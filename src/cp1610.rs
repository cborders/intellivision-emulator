use {
    crate::dasm::Disassembler,
    crate::memory::{Memory, Signals},
    anyhow::{anyhow, Context, Error},
    parking_lot::RwLock,
    std::{
        thread,
        time::{Duration, Instant},
    },
};

// 894886.25 Hz (NTSC)
// 1MHz (PAL/SECAM) 1000000.0 Hz
const CLOCK_SPEED: f64 = 894886.25;
const TARGET_TICK_TIME: Duration = Duration::from_secs_f64(1.0 / CLOCK_SPEED);

/// Implementation of the CP1610 CPU from the Intellivision console. This implementation is based
/// on the information found at http://wiki.intellivision.us/index.php/CP1610
pub struct CP1610 {
    /// Registers. R7 is the program counter and will auto-increment on indirect reads and writes.
    /// R6 is the stack pointer and will auto-increment on indirect reads and auto-decrement on
    /// indirect writes. R4 and R5 are general purpose registers but they will auto-increment on
    /// indirect reads and writes. The rest are normal general purpose registers.
    reg: [u16; 8],

    /// Previous operation resulted in negative value, determined by a one in bit 15 of the result
    sign: bool,
    /// Previous operation resulted in unsigned integer overflow
    carry: bool,
    /// Previous operation resulted in zero
    zero: bool,
    /// Previous operation gave a signed result that is inconsistent with the signs of the source
    /// operands (Signed Overflow)
    overflow: bool,
    /// Allows the INTRM line to trigger an interrupt, causing the CPU to jump to the interrupt
    /// subroutine located in the Executive ROM at $1004
    int_enable: bool,
    /// Causes the next instruction to read a 16-bit operand with two 8-bit memory accesses,
    /// if it supports it
    double_byte: bool,

    /// Is the processor halted
    is_halted: bool,
    /// Keep track of when to reset double_byte
    double_byte_count: u8,
    /// Keep track of whether the previous instruction was interruptable
    interruptable: bool,
    needs_init: bool,

    /// Keep track of how many cycles are left until moving on to the next instruction
    instruction_start: Instant,
    instruction_duration: Duration,

    /// Should print debug information
    debug: bool,
    /// Disassembler
    dasm: Disassembler,
}

impl CP1610 {
    /// Create a new instance of the CP1610
    pub fn new(debug: bool) -> CP1610 {
        CP1610 {
            // Initialize program counter to 0x1000
            reg: [0, 0, 0, 0, 0, 0, 0, 0x1000],
            sign: false,
            carry: false,
            zero: false,
            overflow: false,
            int_enable: false,
            double_byte: false,
            is_halted: false,
            double_byte_count: 0,
            interruptable: false,
            needs_init: true,
            instruction_start: Instant::now(),
            instruction_duration: Duration::from_millis(0),
            debug,
            dasm: Disassembler::new(),
        }
    }

    /// Advance the CPU one clock cycle
    /// # Parameters
    /// -`memory`: the system memory
    ///
    /// # Errors
    /// - When encountering an unknown opcode
    /// - When trying to access memory that doesn't exist
    pub fn tick(
        &mut self,
        memory_lock: &RwLock<Memory>,
        signals_lock: &RwLock<Signals>,
    ) -> Result<(), Error> {
        if self.needs_init {
            // Init memory
            let mut memory = memory_lock.write();

            // Clear scratchpad RAM
            for address in 0x0100..=0x01EF {
                memory.write(address, 0x0000)?;
            }

            // Clear system RAM
            for address in 0x0200..=0x035F {
                memory.write(address, 0x0000)?;
            }

            self.needs_init = false;
            self.instruction_start = Instant::now();
        }

        if self.instruction_start.elapsed() >= self.instruction_duration && !self.is_halted {
            // Run the next instruction
            self.instruction_start = Instant::now();
            let interrupted = {
                let signals = signals_lock.read();
                signals.interrupt_request_masked
                    && !signals.interrupt_acknowledge
                    && self.int_enable
                    && self.interruptable
            };

            if interrupted {
                // Push PC into the stack
                self.indirect_write(0x06, self.reg[7], memory_lock)?;
                self.reg[7] = 0x1004;
                signals_lock.write().interrupt_acknowledge = true;
            }

            if self.debug {
                println!();
                self.print_regs();
                self.print_flags(interrupted, signals_lock);
                self.print_asm(memory_lock);
            }

            let cycles = self.step(memory_lock)?;
            self.instruction_duration =
                TARGET_TICK_TIME
                    .checked_mul(cycles.into())
                    .unwrap_or_else(|| {
                        eprintln!("Failed to calculate instruction duration");
                        Duration::from_millis(0)
                    });
        }

        thread::yield_now();
        Ok(())
    }

    fn print_regs(&self) {
        // println!(
        //     "R0:{:>04X} R1:{:>04X} R2:{:>04X} R3:{:>04X}",
        //     self.reg[0], self.reg[1], self.reg[2], self.reg[3]
        // );
        // println!(
        //     "R4:{:>04X} R5:{:>04X} R6:{:>04X} R7:{:>04X}",
        //     self.reg[4], self.reg[5], self.reg[6], self.reg[7]
        // );

        print!(
            " {:>04X} {:>04X} {:>04X} {:>04X} ",
            self.reg[0], self.reg[1], self.reg[2], self.reg[3]
        );
        print!(
            "{:>04X} {:>04X} {:>04X} {:>04X} ",
            self.reg[4], self.reg[5], self.reg[6], self.reg[7]
        );
    }

    fn print_flags(&self, interrupted: bool, signals_lock: &RwLock<Signals>) {
        print!("{}", if self.sign { "S" } else { "-" });
        print!("{}", if self.zero { "Z" } else { "-" });
        print!("{}", if self.overflow { "O" } else { "-" });
        print!("{}", if self.carry { "C" } else { "-" });
        print!("{}", if self.int_enable { "I" } else { "-" });
        print!("{}", if self.double_byte { "D" } else { "-" });
        print!("{}", if self.interruptable { "i" } else { "-" });

        let interrupt_state = if interrupted {
            "Q"
        } else if signals_lock.read().interrupt_request_masked {
            "q"
        } else {
            "-"
        };

        print!("{}", interrupt_state);
        print!("  ");
    }

    fn print_asm(&mut self, memory_lock: &RwLock<Memory>) {
        let opcode = self.peek_decle(self.reg[7], memory_lock).unwrap();
        let decle2 = self.peek_decle(self.reg[7] + 1, memory_lock);
        let decle3 = self.peek_decle(self.reg[7] + 2, memory_lock);

        self.dasm.pc = (self.reg[7] + 1) as usize;
        self.dasm.double_byte = self.double_byte_count;

        let (command, arg1, arg2, decles_used) = self.dasm.dasm_opcode(opcode, decle2, decle3);
        print!("{}", command);

        if let Some(arg1) = arg1 {
            print!(" {}", arg1);
        }

        if let Some(arg2) = arg2 {
            print!(" {}", arg2);
        }

        print!("; {:>04X}", opcode);

        if decles_used >= 1 {
            print!(" {:>04X}", decle2.unwrap());
        }

        if decles_used >= 2 {
            print!(" {:>04X}", decle3.unwrap());
        }
    }

    fn step(&mut self, memory_lock: &RwLock<Memory>) -> Result<u16, Error> {
        let opcode = self.read_instruction(memory_lock)?;
        let cycles = match opcode {
            0x0000 => self.set_halted()?,
            0x0001 => self.set_double_byte()?,
            0x0002 => self.enable_interrupts(true)?,
            0x0003 => self.enable_interrupts(false)?,
            0x0004 => {
                let decle2 = self.read_instruction(memory_lock)?;
                let decle3 = self.read_instruction(memory_lock)?;
                self.jump(decle2, decle3)?
            }
            0x0005 => {
                // Terminate Current Interrupt
                self.interruptable = false;
                4
            }
            0x0006 => self.set_carry(false)?,
            0x0007 => self.set_carry(true)?,
            0x0008..=0x000F => self.increment_register(opcode, false)?,
            0x0010..=0x0017 => self.decrement_register(opcode)?,
            0x0018..=0x001F => self.compliment_register(opcode)?,
            0x0020..=0x0027 => self.negate_register(opcode)?,
            0x0028..=0x002F => self.add_carry_to_register(opcode)?,
            0x0030..=0x0033 => self.get_status_word(opcode)?,
            0x0034..=0x0035 => {
                // NOP
                self.interruptable = true;
                6
            }
            0x0036..=0x0037 => {
                // Software Interrupt
                self.interruptable = true;
                6
            }
            0x0038..=0x003F => self.return_status_word(opcode)?,
            0x0040..=0x0047 => self.swap_bytes(opcode)?,
            0x0048..=0x004F => self.shift_logical_left(opcode)?,
            0x0050..=0x0057 => self.rotate_left_through_carry(opcode)?,
            0x0058..=0x005F => self.shift_logical_left_through_carry(opcode)?,
            0x0060..=0x0067 => self.shift_logical_right(opcode)?,
            0x0068..=0x006F => self.shift_arithmetic_right(opcode)?,
            0x0070..=0x0077 => self.rotate_right_through_carry(opcode)?,
            0x0078..=0x007F => self.shift_arithmetic_right_through_carry(opcode)?,
            0x0080..=0x00BF => self.move_register(opcode)?,
            0x00C0..=0x00FF => self.add_registers(opcode)?,
            0x0100..=0x013F => self.subtract_registers(opcode)?,
            0x0140..=0x017F => {
                let (_, cycles) = self.compare_registers(opcode)?;
                cycles
            }
            0x0180..=0x01BF => self.and_registers(opcode)?,
            0x01C0..=0x01FF => self.xor_registers(opcode)?,
            0x0200..=0x023F => {
                let decle2 = self.read_instruction(memory_lock)?;
                self.branch(opcode, decle2)?
            }
            0x0240..=0x0247 => {
                let decle2 = self.read_instruction(memory_lock)?;
                self.move_out(opcode, decle2, memory_lock)?
            }
            0x0248..=0x027F => self.move_out_indirect(opcode, memory_lock)?,
            0x0280..=0x0287 => {
                let decle2 = self.read_instruction(memory_lock)?;
                self.move_in(opcode, decle2, memory_lock)?
            }
            0x0288..=0x02BF => self.move_in_indirect(opcode, memory_lock)?,
            0x02C0..=0x02C7 => {
                let decle2 = self.read_instruction(memory_lock)?;
                self.add(opcode, decle2, memory_lock)?
            }
            0x02C8..=0x02FF => self.add_indirect(opcode, memory_lock)?,
            0x0300..=0x0307 => {
                let decle2 = self.read_instruction(memory_lock)?;
                self.subtract(opcode, decle2, memory_lock)?
            }
            0x0308..=0x033F => self.subtract_indirect(opcode, memory_lock)?,
            0x0340..=0x0347 => {
                let decle2 = self.read_instruction(memory_lock)?;
                let (_, cycles) = self.compare(opcode, decle2, memory_lock)?;
                cycles
            }
            0x0348..=0x037F => {
                let (_, cycles) = self.compare_indirect(opcode, memory_lock)?;
                cycles
            }
            0x0380..=0x0387 => {
                let decle2 = self.read_instruction(memory_lock)?;
                self.and(opcode, decle2, memory_lock)?
            }
            0x0388..=0x03BF => self.and_indirect(opcode, memory_lock)?,
            0x03C0..=0x03C7 => {
                let decle2 = self.read_instruction(memory_lock)?;
                self.xor(opcode, decle2, memory_lock)?
            }
            0x03C8..=0x03FF => self.xor_indirect(opcode, memory_lock)?,
            _ => return Err(anyhow!(format!("Invalid opcode: {:>04X}", opcode))),
        };

        self.try_clear_double_byte();
        Ok(cycles)
    }

    fn set_halted(&mut self) -> Result<u16, Error> {
        self.is_halted = true;
        self.interruptable = false;
        Ok(0)
    }

    fn set_double_byte(&mut self) -> Result<u16, Error> {
        self.double_byte = true;
        self.double_byte_count = 2;
        self.interruptable = false;
        Ok(4)
    }

    fn try_clear_double_byte(&mut self) {
        if self.double_byte_count > 0 {
            self.double_byte_count -= 1;
            if self.double_byte_count <= 0 {
                self.double_byte = false;
            }
        }
    }

    fn enable_interrupts(&mut self, enable: bool) -> Result<u16, Error> {
        self.int_enable = enable;
        self.interruptable = false;
        Ok(4)
    }

    fn jump(&mut self, decle2: u16, decle3: u16) -> Result<u16, Error> {
        let (reg, flags, address) = self.parse_jump_args(decle2, decle3);

        match reg {
            0x00..=0x02 => self.reg[reg + 4] = self.reg[7],
            _ => (), // Don't store the return address
        }

        match flags {
            0x00 => (), // Don't change the interrupt flag
            0x01 => self.int_enable = true,
            0x02 => self.int_enable = false,
            _ => return Err(anyhow!("Unknown opcode in jump flags: {:0>2X}", flags)),
        }

        self.reg[7] = address;
        self.interruptable = true;

        Ok(12)
    }

    fn set_carry(&mut self, set: bool) -> Result<u16, Error> {
        self.carry = set;
        self.interruptable = false;
        Ok(4)
    }

    fn increment_register(&mut self, opcode: u16, update_flags: bool) -> Result<u16, Error> {
        let mut value = self
            .decode_and_read_register(opcode)
            .context("Failed to increment register")?;
        value = self.add_impl(value, 1, update_flags);
        self.update_zero_sign(value);

        self.decode_and_write_register(opcode, value)
            .context("Failed to increment register")?;

        self.interruptable = true;
        Ok(6)
    }

    fn decrement_register(&mut self, opcode: u16) -> Result<u16, Error> {
        let mut value = self
            .decode_and_read_register(opcode)
            .context("Failed to decrement register")?;
        value = self.subtract_impl(value, 1, false);
        self.update_zero_sign(value);

        self.decode_and_write_register(opcode, value)
            .context("Failed to decrement register")?;

        self.interruptable = true;
        Ok(6)
    }

    fn compliment_register(&mut self, opcode: u16) -> Result<u16, Error> {
        let mut value = self
            .decode_and_read_register(opcode)
            .context("Failed to compliment register")?;

        value ^= 0xFFFF;
        self.update_zero_sign(value);

        self.decode_and_write_register(opcode, value)
            .context("Failed to compliment register")?;

        self.interruptable = true;
        Ok(6)
    }

    fn negate_register(&mut self, opcode: u16) -> Result<u16, Error> {
        let mut value = self
            .decode_and_read_register(opcode)
            .context("Failed to negate register")?;

        value ^= 0xFFFF;
        let value = self.add_impl(value, 1, true);
        self.update_zero_sign(value);

        self.decode_and_write_register(opcode, value)
            .context("Failed to negate register")?;

        self.interruptable = true;
        Ok(6)
    }

    fn add_carry_to_register(&mut self, opcode: u16) -> Result<u16, Error> {
        if self.carry {
            self.increment_register(opcode, true)?;
        }
        self.interruptable = true;
        Ok(6)
    }

    fn get_status_word(&mut self, opcode: u16) -> Result<u16, Error> {
        let mut status: u16 = 0;
        if self.sign {
            status |= 0x08;
        }
        if self.zero {
            status |= 0x04;
        }
        if self.overflow {
            status |= 0x02;
        }
        if self.carry {
            status |= 0x01;
        }

        let value = ((status << 8) | status) << 4;

        self.decode_and_write_register_subset(opcode, value)
            .context("Failed to get status word")?;

        self.interruptable = true;
        Ok(6)
    }

    fn return_status_word(&mut self, opcode: u16) -> Result<u16, Error> {
        let value = self
            .decode_and_read_register(opcode)
            .context("Failed to return status word")?;

        self.sign = value & 0x0080 != 0;
        self.zero = value & 0x0040 != 0;
        self.overflow = value & 0x0020 != 0;
        self.carry = value & 0x0010 != 0;
        self.interruptable = true;
        Ok(6)
    }

    fn swap_bytes(&mut self, opcode: u16) -> Result<u16, Error> {
        let mut value = self
            .decode_and_read_register_subset(opcode)
            .context("Failed to swap bytes")?;

        let cycles = if opcode & 0x0004 == 0 {
            // Single swap
            let low = value & 0x00FF;
            let high = value & 0xFF00;
            value = low << 8 | high >> 8;
            6
        } else {
            // Double swap
            let low = value & 0x00FF;
            value = low << 8 | low;
            8
        };

        self.zero = value == 0;
        self.sign = (value & 0x0080) != 0;
        self.decode_and_write_register_subset(opcode, value)
            .context("Failed to swap bytes")?;

        self.interruptable = false;
        Ok(cycles)
    }

    fn shift_logical_left(&mut self, opcode: u16) -> Result<u16, Error> {
        let mut value = self
            .decode_and_read_register_subset(opcode)
            .context("Failed to shift logical left")?;

        let cycles = if opcode & 0x0004 == 0 {
            // Shift once
            value <<= 1;
            6
        } else {
            // Shift twice
            value <<= 2;
            8
        };

        self.update_zero_sign(value);
        self.decode_and_write_register_subset(opcode, value)
            .context("Failed to shift logical left")?;

        self.interruptable = false;
        Ok(cycles)
    }

    fn rotate_left_through_carry(&mut self, opcode: u16) -> Result<u16, Error> {
        let mut value = self
            .decode_and_read_register_subset(opcode)
            .context("Failed to rotate left through carry")?;

        let cycles = if opcode & 0x0004 == 0 {
            // Rotate once
            let old_carry: u16 = if self.carry { 0x01 } else { 0x0 };
            self.carry = value & 0x8000 != 0;
            value <<= 1;
            value |= old_carry;
            6
        } else {
            // Rotate twice
            let mut shift_in: u16 = 0;
            if self.carry {
                shift_in |= 0x02;
            }
            if self.overflow {
                shift_in |= 0x01;
            }
            self.carry = value & 0x8000 != 0;
            self.overflow = value & 0x4000 != 0;
            value <<= 2;
            value |= shift_in;
            8
        };

        self.update_zero_sign(value);
        self.decode_and_write_register_subset(opcode, value)
            .context("Failed to rotate left through carry")?;

        self.interruptable = false;
        Ok(cycles)
    }

    fn shift_logical_left_through_carry(&mut self, opcode: u16) -> Result<u16, Error> {
        let mut value = self
            .decode_and_read_register_subset(opcode)
            .context("Failed to shift logical left through carry")?;

        let cycles = if opcode & 0x0004 == 0 {
            // Rotate once
            self.carry = value & 0x8000 != 0;
            value <<= 1;
            6
        } else {
            // Rotate twice
            self.carry = value & 0x8000 != 0;
            self.overflow = value & 0x4000 != 0;
            value <<= 2;
            8
        };

        self.update_zero_sign(value);
        self.decode_and_write_register_subset(opcode, value)
            .context("Failed to shift logical left through carry")?;

        self.interruptable = false;
        Ok(cycles)
    }

    fn shift_logical_right(&mut self, opcode: u16) -> Result<u16, Error> {
        let mut value = self
            .decode_and_read_register_subset(opcode)
            .context("Failed to shift logical right")?;

        let cycles = if opcode & 0x0004 == 0 {
            // Shift once
            value >>= 1;
            6
        } else {
            // Shift twice
            value >>= 2;
            8
        };

        self.zero = value == 0;
        self.sign = (value & 0x0080) != 0;
        self.decode_and_write_register_subset(opcode, value)
            .context("Failed to shift logical right")?;

        self.interruptable = false;
        Ok(cycles)
    }

    fn shift_arithmetic_right(&mut self, opcode: u16) -> Result<u16, Error> {
        let mut value = self
            .decode_and_read_register_subset(opcode)
            .context("Failed to shift arithmetic right")?;

        let cycles = if opcode & 0x0004 == 0 {
            // Shift once
            let shift_in = value & 0x8000;
            value >>= 1;
            value |= shift_in;
            6
        } else {
            // Shift twice
            let shift_in = if value & 0x8000 != 0 { 0xC000 } else { 0x0000 };
            value >>= 2;
            value |= shift_in;
            8
        };

        self.zero = value == 0;
        self.sign = (value & 0x0080) != 0;
        self.decode_and_write_register_subset(opcode, value)
            .context("Failed to shift arithmetic right")?;

        self.interruptable = false;
        Ok(cycles)
    }

    fn rotate_right_through_carry(&mut self, opcode: u16) -> Result<u16, Error> {
        let mut value = self
            .decode_and_read_register_subset(opcode)
            .context("Failed to rotate right through carry")?;

        let cycles = if opcode & 0x0004 == 0 {
            // Rotate once
            let shift_in: u16 = if self.carry { 0x8000 } else { 0x0 };
            self.carry = value & 0x0001 != 0;
            value >>= 1;
            value |= shift_in;
            6
        } else {
            // Rotate twice
            let mut shift_in: u16 = 0;
            if self.carry {
                shift_in |= 0x4000;
            }
            if self.overflow {
                shift_in |= 0x8000;
            }
            self.carry = value & 0x0001 != 0;
            self.overflow = value & 0x0002 != 0;
            value >>= 2;
            value |= shift_in;
            8
        };

        self.zero = value == 0;
        self.sign = (value & 0x0080) != 0;
        self.decode_and_write_register_subset(opcode, value)
            .context("Failed to rotate right through carry")?;

        self.interruptable = false;
        Ok(cycles)
    }

    fn shift_arithmetic_right_through_carry(&mut self, opcode: u16) -> Result<u16, Error> {
        let mut value = self
            .decode_and_read_register_subset(opcode)
            .context("Failed to shift arithmetic right through carry")?;

        let cycles = if opcode & 0x0004 == 0 {
            // Shift once
            let shift_in = value & 0x8000;
            self.carry = value & 0x0001 != 0;
            value >>= 1;
            value |= shift_in;
            6
        } else {
            // Shift twice
            let shift_in = if value & 0x8000 != 0 { 0xC000 } else { 0x0000 };
            self.carry = value & 0x0001 != 0;
            self.overflow = value & 0x0002 != 0;
            value >>= 2;
            value |= shift_in;
            8
        };

        self.zero = value == 0;
        self.sign = (value & 0x0080) != 0;
        self.decode_and_write_register_subset(opcode, value)
            .context("Failed to shift arithmetic right through carry")?;

        self.interruptable = false;
        Ok(cycles)
    }

    fn move_register(&mut self, opcode: u16) -> Result<u16, Error> {
        let source = ((opcode >> 3) & 0x0007) as usize;
        let value = self.read_register(source)?;

        let dest = (opcode & 0x0007) as usize;
        match dest {
            0x00..=0x07 => self.reg[dest] = value,
            _ => return Err(anyhow!("Unknown register: {}", dest)),
        }

        self.update_zero_sign(value);
        self.interruptable = true;
        match dest {
            0x00..=0x05 => Ok(6),
            0x06..=0x07 => Ok(7),
            _ => Err(anyhow!("Unknown register: {}", dest)),
        }
    }

    fn add_registers(&mut self, opcode: u16) -> Result<u16, Error> {
        let (source_value, dest_value) = self.decode_and_read_source_and_dest_registers(opcode)?;
        let value = self.add_impl(dest_value, source_value, true);
        self.decode_and_write_register(opcode, value)?;
        self.update_zero_sign(value);
        self.interruptable = true;
        Ok(6)
    }

    fn subtract_registers(&mut self, opcode: u16) -> Result<u16, Error> {
        // compare_registers does everything that subtract_registers needs except writing the value
        // to memory so just use that and then write the value here.
        let (value, cycles) = self.compare_registers(opcode)?;
        self.decode_and_write_register(opcode, value)?;
        self.interruptable = true;
        Ok(cycles)
    }

    fn compare_registers(&mut self, opcode: u16) -> Result<(u16, u16), Error> {
        let (source_value, dest_value) = self.decode_and_read_source_and_dest_registers(opcode)?;
        let value = self.subtract_impl(dest_value, source_value, true);
        self.update_zero_sign(value);
        self.interruptable = true;
        Ok((value, 6))
    }

    fn and_registers(&mut self, opcode: u16) -> Result<u16, Error> {
        let (source_value, dest_value) = self.decode_and_read_source_and_dest_registers(opcode)?;
        let value = source_value & dest_value;
        self.decode_and_write_register(opcode, value)?;
        self.update_zero_sign(value);
        self.interruptable = true;
        Ok(6)
    }

    fn xor_registers(&mut self, opcode: u16) -> Result<u16, Error> {
        let (source_value, dest_value) = self.decode_and_read_source_and_dest_registers(opcode)?;
        let value = source_value ^ dest_value;
        self.decode_and_write_register(opcode, value)?;
        self.update_zero_sign(value);
        self.interruptable = true;
        Ok(6)
    }

    fn branch(&mut self, opcode: u16, decle2: u16) -> Result<u16, Error> {
        let forward = opcode & 0x0020 == 0;
        let external = opcode & 0x0010 != 0;
        let negate = opcode & 0x0008 != 0;
        let condition = (opcode & 0x0007) as u8;

        let cycles = if external {
            // This is meant to branch if the external pins EBCA0-EBCA3 match the specified value.
            // These pins are disconnected on the Intellivision so this is basically a nop.
            7
        } else {
            let offset = match condition {
                // Branch Unconditionally
                0x00 if negate => 0,
                0x00 => decle2,
                // Branch on carry
                0x01 if self.carry != negate => decle2,
                0x01 => 0,
                // Branch on overflow
                0x02 if self.overflow != negate => decle2,
                0x02 => 0,
                // Branch on sign cleared
                0x03 if self.sign == negate => decle2,
                0x03 => 0,
                // Branch on zero
                0x04 if self.zero != negate => decle2,
                0x04 => 0,
                // Branch on sign != overflow
                0x05 if (self.sign != self.overflow) != negate => decle2,
                0x05 => 0,
                // Branch on zero | sign != overflow
                0x06 if (self.zero | self.sign != self.overflow) != negate => decle2,
                0x06 => 0,
                // Branch on sign != carry
                0x07 if (self.sign != self.carry) != negate => decle2,
                0x07 => 0,
                _ => return Err(anyhow!("Unknown branch condition: {}", condition)),
            };

            if offset != 0 {
                if forward {
                    self.reg[7] = self.reg[7] + offset;
                } else {
                    self.reg[7] = self.reg[7] - (offset + 1);
                }
                9
            } else {
                7
            }
        };

        self.interruptable = true;
        Ok(cycles)
    }

    fn move_out(
        &mut self,
        opcode: u16,
        decle2: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<u16, Error> {
        let address = decle2 as usize;
        let value = self.decode_and_read_register(opcode)?;
        memory_lock.write().write(address, value)?;
        self.interruptable = false;
        Ok(11)
    }

    fn move_out_indirect(
        &mut self,
        opcode: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<u16, Error> {
        let (address_reg, source_reg) = self.decode_address_and_data_registers(opcode);
        let value = self.read_register(source_reg)?;
        self.indirect_write(address_reg, value, memory_lock)?;
        self.interruptable = false;
        Ok(9)
    }

    fn move_in(
        &mut self,
        opcode: u16,
        decle2: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<u16, Error> {
        let address = decle2 as usize;
        let value = memory_lock.read().read(address)?;
        self.decode_and_write_register(opcode, value)?;
        self.interruptable = true;
        Ok(10)
    }

    fn move_in_indirect(
        &mut self,
        opcode: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<u16, Error> {
        let (address_reg, dest_reg) = self.decode_address_and_data_registers(opcode);
        let value = self.indirect_read(address_reg, memory_lock)?;

        match dest_reg {
            0x00..=0x07 => self.reg[dest_reg] = value,
            _ => return Err(anyhow!("Unknown register: {}", dest_reg)),
        };

        let cycles = match address_reg {
            _ if self.double_byte => 10,
            0x06 => 11,
            _ => 8,
        };

        self.interruptable = true;
        Ok(cycles)
    }

    fn add(
        &mut self,
        opcode: u16,
        decle2: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<u16, Error> {
        let lvalue = self.decode_and_read_register(opcode)?;
        let address = decle2 as usize;
        let rvalue = memory_lock.read().read(address)?;
        let value = self.add_impl(lvalue, rvalue, true);
        self.decode_and_write_register(opcode, value)?;
        self.update_zero_sign(value);
        self.interruptable = true;
        Ok(10)
    }

    fn add_indirect(&mut self, opcode: u16, memory_lock: &RwLock<Memory>) -> Result<u16, Error> {
        let (lvalue, rvalue) = self.read_lvalue_rvalue(opcode, memory_lock)?;
        let value = self.add_impl(lvalue, rvalue, true);

        let cycles = match self.write_bin_op_result(opcode, value)? {
            _ if self.double_byte => 10,
            0x06 => 11,
            _ => 8,
        };
        self.update_zero_sign(value);
        self.interruptable = true;
        Ok(cycles)
    }

    fn subtract(
        &mut self,
        opcode: u16,
        decle2: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<u16, Error> {
        // Compare already does everything except actually write out the value so use that.
        let (value, cycles) = self.compare(opcode, decle2, memory_lock)?;
        self.decode_and_write_register(opcode, value)?;
        self.interruptable = true;
        Ok(cycles)
    }

    fn subtract_indirect(
        &mut self,
        opcode: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<u16, Error> {
        // Compare Indirect already does everything except actually write out the value so use that.
        let (value, cycles) = self.compare_indirect(opcode, memory_lock)?;
        let _ = self.write_bin_op_result(opcode, value)?;
        self.interruptable = true;
        Ok(cycles)
    }

    fn compare(
        &mut self,
        opcode: u16,
        decle2: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<(u16, u16), Error> {
        let lvalue = self.decode_and_read_register(opcode)?;
        let address = decle2 as usize;
        let rvalue = memory_lock.read().read(address)?;
        let value = self.subtract_impl(lvalue, rvalue, true);
        self.update_zero_sign(value);
        self.interruptable = true;
        Ok((value, 10))
    }

    fn compare_indirect(
        &mut self,
        opcode: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<(u16, u16), Error> {
        let (lvalue, rvalue) = self.read_lvalue_rvalue(opcode, memory_lock)?;
        let value = self.subtract_impl(lvalue, rvalue, true);
        let (_, dest_reg) = self.decode_address_and_data_registers(opcode);
        let cycles = match dest_reg {
            _ if self.double_byte => 10,
            0x06 => 11,
            _ => 8,
        };
        self.update_zero_sign(value);
        self.interruptable = true;
        Ok((value, cycles))
    }

    fn and(
        &mut self,
        opcode: u16,
        decle2: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<u16, Error> {
        let lvalue = self.decode_and_read_register(opcode)?;
        let address = decle2 as usize;
        let rvalue = memory_lock.read().read(address)?;
        let value = lvalue & rvalue;
        self.decode_and_write_register(opcode, value)?;
        self.update_zero_sign(value);
        self.interruptable = true;
        Ok(10)
    }

    fn and_indirect(&mut self, opcode: u16, memory_lock: &RwLock<Memory>) -> Result<u16, Error> {
        let (lvalue, rvalue) = self.read_lvalue_rvalue(opcode, memory_lock)?;
        let value = lvalue & rvalue;

        let cycles = match self.write_bin_op_result(opcode, value)? {
            _ if self.double_byte => 10,
            0x06 => 11,
            _ => 8,
        };
        self.update_zero_sign(value);
        self.interruptable = true;
        Ok(cycles)
    }

    fn xor(
        &mut self,
        opcode: u16,
        decle2: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<u16, Error> {
        let lvalue = self.decode_and_read_register(opcode)?;
        let address = decle2 as usize;
        let rvalue = memory_lock.read().read(address)?;
        let value = lvalue ^ rvalue;
        self.decode_and_write_register(opcode, value)?;
        self.update_zero_sign(value);
        self.interruptable = true;
        Ok(10)
    }

    fn xor_indirect(&mut self, opcode: u16, memory_lock: &RwLock<Memory>) -> Result<u16, Error> {
        let (lvalue, rvalue) = self.read_lvalue_rvalue(opcode, memory_lock)?;
        let value = lvalue ^ rvalue;

        let cycles = match self.write_bin_op_result(opcode, value)? {
            _ if self.double_byte => 10,
            0x06 => 11,
            _ => 8,
        };
        self.update_zero_sign(value);
        self.interruptable = true;
        Ok(cycles)
    }

    /// Reads the current instruction and increments the program counter
    /// # Parameters
    /// -`memory`: the system memory containing the instructions
    ///
    /// # Returns
    /// - The instruction pointed to by the program counter
    ///
    /// # Errors
    /// - When trying to read an invalid memory location
    fn read_instruction(&mut self, memory_lock: &RwLock<Memory>) -> Result<u16, Error> {
        let old_pc = self.reg[7] as usize;
        self.reg[7] += 1;
        memory_lock.read().read(old_pc)
    }

    fn peek_decle(&self, location: u16, memory_lock: &RwLock<Memory>) -> Option<u16> {
        memory_lock.read().read(location as usize).ok()
    }

    /// Parses the arguments to a jump
    /// # Parameters
    /// - `decle2`: The second decle in the jump instruction
    /// - `decle3`: The third decle in the jump instruction
    ///
    /// # Returns
    /// - A value representing the regisiter to store the return address
    /// - Flags indicating how to handle the interrupt flag
    /// - The address to jump to
    fn parse_jump_args(&self, decle2: u16, decle3: u16) -> (usize, u8, u16) {
        let reg = (decle2 >> 8) as usize;
        let flags = (decle2 & 0x0003) as u8;
        let address = ((decle2 << 8) & 0xFC00) | (decle3 & 0x03FF);
        (reg, flags, address)
    }

    fn decode_address_and_data_registers(&self, opcode: u16) -> (usize, usize) {
        let address = ((opcode >> 3) & 0x0007) as usize;
        let data = (opcode & 0x0007) as usize;
        (address, data)
    }

    fn read_register(&self, register: usize) -> Result<u16, Error> {
        match register {
            0x00..=0x07 => Ok(self.reg[register]),
            _ => Err(anyhow!("Unknown register: {}", register)),
        }
    }

    fn write_register(&mut self, register: usize, value: u16) -> Result<(), Error> {
        match register {
            0x00..=0x07 => {
                self.reg[register] = value;
                Ok(())
            }
            _ => return Err(anyhow!("Unknown register: {}", register)),
        }
    }

    /// Decodes the which register is indicated in the opcode and returns the value held in that
    /// register.
    /// # Parameters
    /// -`opcode`: the opcode encoding which register to read
    ///
    /// # Returns
    /// - The value in the encoded register
    ///
    /// # Errors
    /// - When the encoded register is invalid
    fn decode_and_read_register(&self, opcode: u16) -> Result<u16, Error> {
        let register = (opcode & 0x0007) as usize;
        self.read_register(register)
    }

    /// Decodes the which register is indicated in the opcode and returns the value held in that
    /// register. This version of the function is used when opcodes only encode the register with
    /// two bits limiting the options to R0 - R3
    /// # Parameters
    /// -`opcode`: the opcode encoding which register to read
    ///
    /// # Returns
    /// - The value in the encoded register
    ///
    /// # Errors
    /// - When the encoded register is invalid
    fn decode_and_read_register_subset(&self, opcode: u16) -> Result<u16, Error> {
        let register = (opcode & 0x0003) as usize;
        match register {
            0x00..=0x03 => Ok(self.reg[register]),
            _ => Err(anyhow!("Unknown register: {}", register)),
        }
    }

    /// Decodes the source and destination registers indicated in the opcode and returns the values
    /// in those registers.
    /// # Parameters
    /// -`opcode`: the opcode encoding which registers to read
    ///
    /// # Returns
    /// -`source_value`: the value held in the source register
    /// -`dest_value`: the value held in the destination register
    ///
    /// # Errors
    /// - When either of the encoded registers are invalid
    fn decode_and_read_source_and_dest_registers(&self, opcode: u16) -> Result<(u16, u16), Error> {
        let source = ((opcode >> 3) & 0x0007) as usize;
        let source_value = self.read_register(source)?;

        let dest = (opcode & 0x0007) as usize;
        let dest_value = self.read_register(dest)?;

        Ok((source_value, dest_value))
    }

    /// Decodes the which register is indicated in the opcode and writes the value to that
    /// register.
    /// # Parameters
    /// -`opcode`: the opcode encoding which register to read
    /// -`value`: the value to write to the register
    ///
    /// # Errors
    /// - When the encoded register is invalid
    fn decode_and_write_register(&mut self, opcode: u16, value: u16) -> Result<(), Error> {
        let register = (opcode & 0x0007) as usize;
        self.write_register(register, value)
    }

    /// Decodes the which register is indicated in the opcode and writes the value to that
    /// register. This version of the function is used when opcodes only encode the register with
    /// two bits limiting the options to R0 - R3
    /// # Parameters
    /// -`opcode`: the opcode encoding which register to read
    /// -`value`: the value to write to the register
    ///
    /// # Errors
    /// - When the encoded register is invalid
    fn decode_and_write_register_subset(&mut self, opcode: u16, value: u16) -> Result<(), Error> {
        let register = (opcode & 0x0003) as usize;
        match register {
            0x00..=0x03 => self.reg[register] = value,
            _ => return Err(anyhow!("Unknown register: {}", register)),
        }

        Ok(())
    }

    fn update_zero_sign(&mut self, value: u16) {
        self.zero = value == 0;
        self.sign = value & 0x8000 != 0;
    }

    fn read_lvalue_rvalue(
        &mut self,
        opcode: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<(u16, u16), Error> {
        let (address_reg, dest_reg) = self.decode_address_and_data_registers(opcode);

        // Do the indirect read first incase that changes any register values
        let rvalue = self.indirect_read(address_reg, memory_lock)?;
        let lvalue = self.read_register(dest_reg)?;

        Ok((lvalue, rvalue))
    }

    fn read_double_byte(
        &mut self,
        address_reg: usize,
        memory_lock: &RwLock<Memory>,
    ) -> Result<u16, Error> {
        // If this is a stack read, pre-decrement the stack pointer
        if address_reg == 0x06 {
            self.reg[address_reg] -= 1;
        }

        let lsb = memory_lock.read().read(self.reg[address_reg] as usize)? & 0x00FF;

        // Auto-adjust registers
        match address_reg {
            0x04 | 0x05 | 0x07 => self.reg[address_reg] += 1,
            0x06 => self.reg[address_reg] -= 1,
            _ => (),
        };

        let msb = (memory_lock.read().read(self.reg[address_reg] as usize)? & 0x00FF) << 8;

        // Auto-increment registers
        match address_reg {
            0x04 | 0x05 | 0x07 => self.reg[address_reg] += 1,
            _ => (),
        };

        Ok(msb | lsb)
    }

    fn indirect_read(
        &mut self,
        address_reg: usize,
        memory_lock: &RwLock<Memory>,
    ) -> Result<u16, Error> {
        if address_reg > 0x07 {
            Err(anyhow!("Unknown register: {}", address_reg))
        } else {
            match address_reg {
                0x07 => {
                    // Immediate Read
                    if self.double_byte {
                        let lsb = self.read_instruction(memory_lock)?;
                        let msb = self.read_instruction(memory_lock)?;
                        Ok((msb << 8) | (lsb & 0x00FF))
                    } else {
                        self.read_instruction(memory_lock)
                    }
                }
                _ => {
                    if self.double_byte {
                        self.read_double_byte(address_reg, memory_lock)
                    } else {
                        // If this is a stack read, pre-decrement the stack location
                        if address_reg == 0x06 {
                            self.reg[0x06] -= 1;
                        }

                        let value = memory_lock.read().read(self.reg[address_reg] as usize)?;

                        // Auto-increment registers
                        match address_reg {
                            0x04 | 0x05 => self.reg[address_reg] += 1,
                            _ => (),
                        };

                        Ok(value)
                    }
                }
            }
        }
    }

    fn indirect_write(
        &mut self,
        address_reg: usize,
        value: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<(), Error> {
        let address = match address_reg {
            0x01..=0x06 => self.reg[address_reg] as usize,
            0x07 => (self.reg[address_reg] - 1) as usize,
            _ => return Err(anyhow!("Unknown register: {}", address_reg)),
        };

        match address_reg {
            0x06 => {
                // Stack Write
                memory_lock.write().write(address, value)?;
                self.reg[0x06] += 1;
            }
            0x07 => {
                // Immediate Write
                println!("!!!!! Immediate Mode Write !!!!!");
                memory_lock.write().write(address, value)?;
                self.reg[0x07] += 1;
            }
            _ => {
                memory_lock.write().write(address, value)?;
            }
        }

        // Auto-adjust registers
        match address_reg {
            0x04 | 0x05 => self.reg[address_reg] += 1,
            _ => (),
        };

        Ok(())
    }

    /// Write the result to the decoded register and return the register code.
    fn write_bin_op_result(&mut self, opcode: u16, value: u16) -> Result<usize, Error> {
        let (_, dest_reg) = self.decode_address_and_data_registers(opcode);
        self.write_register(dest_reg, value)?;
        Ok(dest_reg)
    }

    fn add_impl(&mut self, lvalue: u16, rvalue: u16, update_flags: bool) -> u16 {
        let value = if (0xFFFF - lvalue) < rvalue {
            if update_flags {
                self.carry = true;
            }
            rvalue - (0xFFFF - lvalue + 1)
        } else {
            if update_flags {
                self.carry = false;
            }
            lvalue + rvalue
        };

        if update_flags {
            let lsign = lvalue & 0x8000;
            let rsign = rvalue & 0x8000;
            let vsign = value & 0x8000;
            self.overflow = lsign != vsign && rsign != vsign;
        }

        value
    }

    fn subtract_impl(&mut self, lvalue: u16, rvalue: u16, update_flags: bool) -> u16 {
        let value = if rvalue > lvalue {
            if update_flags {
                self.carry = false;
            }
            0xFFFF - (rvalue - lvalue - 1)
        } else {
            if update_flags {
                self.carry = true;
            }
            lvalue - rvalue
        };

        if update_flags {
            let lsign = lvalue & 0x8000;
            let rsign = rvalue & 0x8000;
            let vsign = value & 0x8000;

            self.overflow = if rsign == 0 {
                if lsign == 0 {
                    false
                } else {
                    vsign == 0
                }
            } else {
                if lsign == 0 {
                    vsign != 0
                } else {
                    false
                }
            }
        }

        value
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cpu_init_state() {
        let cpu = CP1610::new();

        assert_eq!(cpu.reg, [0, 0, 0, 0, 0, 0, 0, 0x1000]);
        assert_eq!(cpu.sign, false);
        assert_eq!(cpu.carry, false);
        assert_eq!(cpu.zero, false);
        assert_eq!(cpu.overflow, false);
        assert_eq!(cpu.int_enable, false);
        assert_eq!(cpu.double_byte, false);
        assert_eq!(cpu.is_halted, false);
        assert_eq!(cpu.double_byte_count, 0);
        assert_eq!(cpu.spin_cycles, 0);
        assert_eq!(cpu.interruptable, false);
    }

    #[test]
    fn test_jump() {
        let mut memory = Memory::new();
        let mut signals = Signals::new();

        // Format - Decle #1    Format - Decle #2    Format - Decle #3
        // 0000:0000:0000:0100  0000:00rr:aaaa:aaff  0000:00aa:aaaa:aaaa
        //
        // where:
        // rr    indicates the register into which to store the return address
        // such that:
        //   rr == 00    indicates to store return address in register R4
        //   rr == 01    indicates register R5
        //   rr == 10    indicates register R6
        //   rr == 11    indicates that the CP1610 should not store the
        //               return address, signaling a Jump without return
        // ff    indicates how to affect the Interrupt (I) flag in the CP1610
        // such that:
        //   ff == 00    indicates not to affect the Interrupt flag
        //   ff == 01    indicates to set the Interrupt flag
        //   ff == 10    indicates to clear the Interrupt flag
        //   ff == 11    unknown opcode (behavior unknown!!)
        //
        // aaaaaaaaaaaaaaaa    indicates the address to where the CP1610 should Jump

        // Write a jump as the first instruction to be executed where the return address is stored
        // in R4, the interrupt flag it set, and it jumps to 0x1234
        let opcode = 0x04;
        let decle2 = 0b0000_0000_0001_0001;
        let decle3 = 0b0000_0010_0011_0100;
        memory
            .write(0x1000, opcode)
            .expect("Failed to write to memory");
        memory
            .write(0x1001, decle2)
            .expect("Failed to write to memory");
        memory
            .write(0x1002, decle3)
            .expect("Failed to write to memory");

        let mut cpu = CP1610::new();
        cpu.tick(&mut memory, &mut signals)
            .expect("Failed to tick cpu");

        assert_eq!(cpu.reg, [0, 0, 0, 0, 0x1003, 0, 0, 0x1234]);
        assert_eq!(cpu.int_enable, true);
        assert_eq!(cpu.spin_cycles, 11);
        assert_eq!(cpu.interruptable, true);

        // Write a jump as the first instruction to be executed where the return address is stored
        // in R5, the interrupt flag is cleared, and it jumps to 0x4321
        let decle2 = 0b0000_0001_0100_0010;
        let decle3 = 0b0000_0011_0010_0001;
        memory
            .write(0x1001, decle2)
            .expect("Failed to write to memory");
        memory
            .write(0x1002, decle3)
            .expect("Failed to write to memory");

        let mut cpu = CP1610::new();
        cpu.int_enable = true;
        cpu.tick(&mut memory, &mut signals)
            .expect("Failed to tick cpu");

        assert_eq!(cpu.reg, [0, 0, 0, 0, 0, 0x1003, 0, 0x4321]);
        assert_eq!(cpu.int_enable, false);
        assert_eq!(cpu.spin_cycles, 11);
        assert_eq!(cpu.interruptable, true);

        // Write a jump as the first instruction to be executed where the return address is stored
        // in R6, the interrupt flag is left alone, and it jumps to 0xDEAD
        let decle2 = 0b0000_0010_1101_1100;
        let decle3 = 0b0000_0010_1010_1101;
        memory
            .write(0x1001, decle2)
            .expect("Failed to write to memory");
        memory
            .write(0x1002, decle3)
            .expect("Failed to write to memory");

        let mut cpu = CP1610::new();
        cpu.int_enable = true;
        cpu.tick(&mut memory, &mut signals)
            .expect("Failed to tick cpu");

        assert_eq!(cpu.reg, [0, 0, 0, 0, 0, 0, 0x1003, 0xDEAD]);
        assert_eq!(cpu.int_enable, true);
        assert_eq!(cpu.spin_cycles, 11);
        assert_eq!(cpu.interruptable, true);

        // Write a jump as the first instruction to be executed where the return address is not
        // stored, the interrupt flag is left alone, and it jumps to 0xBEEF
        let decle2 = 0b0000_0011_1011_1100;
        let decle3 = 0b0000_0010_1110_1111;
        memory
            .write(0x1001, decle2)
            .expect("Failed to write to memory");
        memory
            .write(0x1002, decle3)
            .expect("Failed to write to memory");

        let mut cpu = CP1610::new();
        cpu.int_enable = true;
        cpu.tick(&mut memory, &mut signals)
            .expect("Failed to tick cpu");

        assert_eq!(cpu.reg, [0, 0, 0, 0, 0, 0, 0, 0xBEEF]);
        assert_eq!(cpu.int_enable, true);
        assert_eq!(cpu.spin_cycles, 11);
        assert_eq!(cpu.interruptable, true);
    }

    #[test]
    fn test_parse_jump_reg() {
        let decle2: u16 = 0x0300;
        let decle3: u16 = 0x0000;

        let expected_reg: usize = 0x03;
        let expected_flags: u8 = 0x00;
        let expected_address: u16 = 0x0000;

        let cpu = CP1610::new();

        let (actual_reg, actual_flags, actual_address) = cpu.parse_jump_args(decle2, decle3);
        assert_eq!(expected_reg, actual_reg);
        assert_eq!(expected_flags, actual_flags);
        assert_eq!(expected_address, actual_address);
    }

    #[test]
    fn test_parse_jump_flags() {
        let decle2: u16 = 0x0003;
        let decle3: u16 = 0x0000;

        let expected_reg: usize = 0x00;
        let expected_flags: u8 = 0x03;
        let expected_address: u16 = 0x0000;

        let cpu = CP1610::new();

        let (actual_reg, actual_flags, actual_address) = cpu.parse_jump_args(decle2, decle3);
        assert_eq!(expected_reg, actual_reg);
        assert_eq!(expected_flags, actual_flags);
        assert_eq!(expected_address, actual_address);
    }

    #[test]
    fn test_parse_jump_address() {
        let decle2: u16 = 0x00FC;
        let decle3: u16 = 0x03FF;

        let expected_reg: usize = 0x00;
        let expected_flags: u8 = 0x00;
        let expected_address: u16 = 0xFFFF;

        let cpu = CP1610::new();

        let (actual_reg, actual_flags, actual_address) = cpu.parse_jump_args(decle2, decle3);
        assert_eq!(expected_reg, actual_reg);
        assert_eq!(expected_flags, actual_flags);
        assert_eq!(expected_address, actual_address);
    }
}
