use {
    crate::{
        memory::{Memory, Signals},
        Debug,
    },
    anyhow::Error,
    parking_lot::RwLock,
    pixels::Pixels,
    std::{
        thread,
        time::{Duration, Instant},
    },
};

// Clock Speed
// 3579545 Hz (NTSC)
// 4000000 Hz (PAL/SECAM)
const CLOCK_SPEED: f64 = 3579545.0;
const TARGET_TICK_TIME: Duration = Duration::from_secs_f64(1.0 / CLOCK_SPEED);

// Frame Rate
// 59.92 Hz (NTSC)
// 50.00 Hz (PAL/SECAM)
const FRAME_RATE: f64 = 59.92;
const TARGET_FRAME_TIME: Duration = Duration::from_secs_f64(1.0 / FRAME_RATE);

const VBLANK1_CLOCKS: u32 = 2900;
const VBLANK2_CLOCKS: u32 = 3796;

const VBLANK1_TIME: Duration = TARGET_TICK_TIME.saturating_mul(VBLANK1_CLOCKS);
const VBLANK2_TIME: Duration = TARGET_TICK_TIME.saturating_mul(VBLANK2_CLOCKS);

const FRAME_DRAW_TIME: Duration =
    TARGET_FRAME_TIME.saturating_sub(VBLANK1_TIME.saturating_add(VBLANK2_TIME));

pub const SCREEN_WIDTH: u32 = 160 * 2;
pub const SCREEN_HEIGHT: u32 = 96 * 2;

// Object Field
// 167 x 105 (NTSC)
// 168 x 104 (PAL)
const OBJECT_FIELD_WIDTH: usize = 167 * 2;
const OBJECT_FIELD_HEIGHT: usize = 105 * 2;
const OBJECT_FIELD_COLOR_BYTES: usize = 3;
const OBJECT_FIELD_AREA: usize = OBJECT_FIELD_WIDTH * OBJECT_FIELD_HEIGHT;
const OBJECT_FIELD_DATA_SIZE: usize = OBJECT_FIELD_AREA * OBJECT_FIELD_COLOR_BYTES;
const OBJECT_FIELD_X_OFFSET: usize = OBJECT_FIELD_WIDTH - SCREEN_WIDTH as usize;
const OBJECT_FIELD_Y_OFFSET: usize = OBJECT_FIELD_HEIGHT - SCREEN_HEIGHT as usize;

const COLLISION_BUFFER_DATA_SIZE: usize = OBJECT_FIELD_AREA;

const BACKTAB_ADDRESS: usize = 0x0200;
const BACKTAB_ROWS: usize = 12;
const BACKTAB_COLS: usize = 20;
const BACKTAB_SIZE: usize = BACKTAB_ROWS * BACKTAB_COLS;
const CARD_WIDTH: usize = 8;
const CARD_HEIGHT: usize = 8;

const GROM_ADDRESS: usize = 0x3000;
const GRAM_ADDRESS: usize = 0x3800;

const MOB_X_REGISTER: usize = 0x0000;
const MOB_Y_REGISTER: usize = 0x0008;
const MOB_ATTR_REGISTER: usize = 0x0010;
const MOB_COLL_REGISTER: usize = 0x0018;

const BORDER_COLOR_REGISTER: usize = 0x002C;
const BORDER_EXTENSION_REGISTER: usize = 0x0032;

const HORZ_DELAY_REGISTER: usize = 0x0030;
const VERT_DELAY_REGISTER: usize = 0x0031;

#[derive(Clone, Copy)]
struct Color([u8; 3]);

impl Color {
    pub const BLACK: Color = Color([0x00, 0x00, 0x00]);
    pub const BLUE: Color = Color([0x00, 0x2D, 0xFF]);
    pub const RED: Color = Color([0xFF, 0x3D, 0x10]);
    pub const TAN: Color = Color([0xC9, 0xCF, 0xAB]);
    pub const DARK_GREEN: Color = Color([0x38, 0x6B, 0x3F]);
    pub const GREEN: Color = Color([0x00, 0xA7, 0x56]);
    pub const YELLOW: Color = Color([0xFA, 0xEA, 0x50]);
    pub const WHITE: Color = Color([0xFF, 0xFC, 0xFF]);
    pub const GRAY: Color = Color([0xBD, 0xAC, 0xC8]);
    pub const CYAN: Color = Color([0x24, 0xB8, 0xFF]);
    pub const ORANGE: Color = Color([0xFF, 0xB4, 0x1F]);
    pub const BROWN: Color = Color([0x54, 0x6E, 0x00]);
    pub const PINK: Color = Color([0xFF, 0x4E, 0x57]);
    pub const LIGHT_BLUE: Color = Color([0xA4, 0x96, 0xFF]);
    pub const YELLOW_GREEN: Color = Color([0x75, 0xCC, 0x80]);
    pub const PURPLE: Color = Color([0xB5, 0x1A, 0x58]);
    pub const CORNFLOWER_BLUE: Color = Color([0x66, 0x99, 0xFF]);

    pub fn from_code(code: u8) -> Color {
        match code {
            0x00 => Color::BLACK,
            0x01 => Color::BLUE,
            0x02 => Color::RED,
            0x03 => Color::TAN,
            0x04 => Color::DARK_GREEN,
            0x05 => Color::GREEN,
            0x06 => Color::YELLOW,
            0x07 => Color::WHITE,
            0x08 => Color::GRAY,
            0x09 => Color::CYAN,
            0x0A => Color::ORANGE,
            0x0B => Color::BROWN,
            0x0C => Color::PINK,
            0x0D => Color::LIGHT_BLUE,
            0x0E => Color::YELLOW_GREEN,
            0x0F => Color::PURPLE,
            _ => {
                //println!("UNKNOWN COLOR: {:>02X}", code);
                Color::CORNFLOWER_BLUE
            }
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Mode {
    FGBG,
    ColorStack,
}

enum State {
    Init,
    Draw,
    VBLANK1,
    VBLANK2,
}

struct FrameData {
    mob_x: [u16; 8],
    mob_y: [u16; 8],
    mob_attr: [u16; 8],
    mob_collision: [u16; 8],
    border_color: Color,
    border_extension_left: bool,
    border_extension_top: bool,
    horz_delay: u16,
    vert_delay: u16,
}

impl FrameData {
    fn new() -> Self {
        Self {
            mob_x: [0x0000; 8],
            mob_y: [0x0000; 8],
            mob_attr: [0x0000; 8],
            mob_collision: [0x0000; 8],
            border_color: Color::RED,
            border_extension_left: false,
            border_extension_top: false,
            horz_delay: 0x0000,
            vert_delay: 0x0000,
        }
    }
}

struct MobData {
    id: usize,
    x: usize,
    y: usize,

    visible: bool,
    priority: bool,
    interactible: bool,

    double_wide: bool,
    flip_x: bool,
    flip_y: bool,
    y_res: bool,
    size: u8,

    grom: bool,
    color: Color,

    card_id: u16,
    card1: Option<[u8; 8]>,
    card2: Option<[u8; 8]>,
}

impl MobData {
    fn load(id: usize, x_reg: u16, y_reg: u16, attr_reg: u16) -> Self {
        let double_wide = x_reg & 0x0400 != 0;
        let visible = x_reg & 0x0200 != 0;
        let interactible = x_reg & 0x0100 != 0;
        let x = (x_reg & 0x00FF) as usize;

        let flip_y = y_reg & 0x0800 != 0;
        let flip_x = y_reg & 0x0400 != 0;
        let size = ((y_reg >> 8) & 0x0007) as u8;
        let y_res = y_reg & 0x0080 != 0;
        let y = (y_reg & 0x007F) as usize;

        let priority = attr_reg & 0x2000 != 0;
        let grom = attr_reg & 0x0800 == 0;
        let card_id = (attr_reg >> 3) & 0x00FF;
        let mut color_code = attr_reg & 0x0007;
        color_code |= (attr_reg >> 9) & 0x0008;
        let color = Color::from_code(color_code as u8);

        Self {
            id,
            x,
            y,
            visible,
            priority,
            interactible,
            double_wide,
            flip_x,
            flip_y,
            y_res,
            size,
            grom,
            color,
            card_id,
            card1: None,
            card2: None,
        }
    }
}

pub struct Stic {
    debug: Debug,
    state: State,
    color_stack_pointer: usize,
    state_start: Instant,
    object_field: [u8; OBJECT_FIELD_DATA_SIZE],
    collision_buffer: [u8; COLLISION_BUFFER_DATA_SIZE],
}

impl Stic {
    pub fn new(debug: Debug) -> Stic {
        Stic {
            debug,
            state: State::Init,
            color_stack_pointer: 0,
            state_start: Instant::now(),
            object_field: [0x00; OBJECT_FIELD_DATA_SIZE],
            collision_buffer: [0xFF; COLLISION_BUFFER_DATA_SIZE],
        }
    }

    fn update_state(&mut self, signals_lock: &RwLock<Signals>) {
        let target_state_duration = match self.state {
            State::Init => return, // Initialization doesn't have a duration assocaited with it
            State::Draw => FRAME_DRAW_TIME,
            State::VBLANK1 => VBLANK1_TIME,
            State::VBLANK2 => VBLANK2_TIME,
        };

        if self.state_start.elapsed() >= target_state_duration {
            match self.state {
                State::Init => (),
                State::Draw => {
                    self.state = State::VBLANK1;
                    signals_lock.write().interrupt_request_masked = true;
                }
                State::VBLANK1 => {
                    self.state = State::VBLANK2;
                }
                State::VBLANK2 => {
                    self.state = State::Draw;
                    self.color_stack_pointer = 0;
                    signals_lock.write().interrupt_request_masked = false;
                }
            }

            self.state_start = Instant::now();
        }
    }

    fn check_interrupt_ack(&self, signals_lock: &RwLock<Signals>) {
        let (intrm, intrm_ack) = {
            let signals = signals_lock.read();
            (
                signals.interrupt_request_masked,
                signals.interrupt_acknowledge,
            )
        };

        if intrm && intrm_ack {
            {
                let mut signals = signals_lock.write();
                signals.interrupt_request_masked = false;
                signals.interrupt_acknowledge = false;
            }
        }
    }

    pub fn draw(
        &mut self,
        screen_lock: &RwLock<Pixels<fltk::window::DoubleWindow>>,
        memory_lock: &RwLock<Memory>,
        signals_lock: &RwLock<Signals>,
    ) -> Result<(), Error> {
        self.update_state(signals_lock);

        match self.state {
            State::Init => {
                // Init memory
                {
                    let mut memory = memory_lock.write();

                    // CLear STIC registers
                    for address in 0x0000..=0x003F {
                        memory.write(address, 0x0000)?;
                    }

                    // Clear graphics RAM
                    for address in 0x3800..=0x3FFF {
                        memory.write(address, 0x0000)?;
                    }
                }

                self.state = State::Draw;
                self.state_start = Instant::now();
            }
            State::Draw => {
                // TODO: Still need to add boder collision
                let mut frame_data = self.get_frame_data(memory_lock)?;
                // Is the display active?
                if self.check_display_active(signals_lock) {
                    self.clear_object_field(&frame_data);
                    self.clear_collision_buffer();
                    self.draw_background(memory_lock, signals_lock)?;
                    self.draw_mobs(&mut frame_data, memory_lock)?;
                    if self.debug == Debug::Collision {
                        self.draw_collision_data();
                    }
                    self.write_mob_collision(&frame_data, memory_lock)?;
                    self.load_frame(&frame_data, screen_lock);
                } else {
                    // The display is NOT active
                    self.load_frame(&frame_data, screen_lock);
                }
            }
            State::VBLANK1 => {
                self.check_interrupt_ack(signals_lock);
            }
            State::VBLANK2 => {
                self.check_interrupt_ack(signals_lock);
            }
        }

        thread::yield_now();
        Ok(())
    }

    fn write_mob_collision(
        &self,
        frame_data: &FrameData,
        memory_lock: &RwLock<Memory>,
    ) -> Result<(), Error> {
        let mut memory = memory_lock.write();
        for index in 0..8 {
            memory.write(MOB_COLL_REGISTER + index, frame_data.mob_collision[index])?;
        }
        Ok(())
    }

    fn get_frame_data(&self, memory_lock: &RwLock<Memory>) -> Result<FrameData, Error> {
        let mut frame_data = FrameData::new();

        frame_data.mob_x = self.read_mob_register(MOB_X_REGISTER, memory_lock)?;
        frame_data.mob_y = self.read_mob_register(MOB_Y_REGISTER, memory_lock)?;
        frame_data.mob_attr = self.read_mob_register(MOB_ATTR_REGISTER, memory_lock)?;
        frame_data.mob_collision = self.read_mob_register(MOB_COLL_REGISTER, memory_lock)?;

        let border_color_code = memory_lock.read().read(BORDER_COLOR_REGISTER)? as u8;
        frame_data.border_color = Color::from_code(border_color_code);

        let border_extension = memory_lock.read().read(BORDER_EXTENSION_REGISTER)?;
        frame_data.border_extension_left = border_extension & 0x0001 != 0;
        frame_data.border_extension_top = border_extension & 0x0002 != 0;

        frame_data.horz_delay = memory_lock.read().read(HORZ_DELAY_REGISTER)?;
        frame_data.vert_delay = memory_lock.read().read(VERT_DELAY_REGISTER)?;

        Ok(frame_data)
    }

    fn read_mob_register(
        &self,
        location: usize,
        memory_lock: &RwLock<Memory>,
    ) -> Result<[u16; 8], Error> {
        let mut registers = [0x0000; 8];
        for index in 0..8 {
            registers[index] = memory_lock.read().read(location + index)?;
        }
        Ok(registers)
    }

    fn clear_object_field(&mut self, frame_data: &FrameData) {
        let Color(border_color) = frame_data.border_color;
        for index in (0..self.object_field.len()).step_by(OBJECT_FIELD_COLOR_BYTES as usize) {
            self.object_field[index] = border_color[0];
            self.object_field[index + 1] = border_color[1];
            self.object_field[index + 2] = border_color[2];
        }
    }

    fn clear_collision_buffer(&mut self) {
        for index in 0..self.collision_buffer.len() {
            self.collision_buffer[index] = 0xFF;
        }
    }

    fn draw_background(
        &mut self,
        memory_lock: &RwLock<Memory>,
        signals_lock: &RwLock<Signals>,
    ) -> Result<(), Error> {
        let mode = { signals_lock.read().stic_mode };

        if self.debug == Debug::BACKTAB {
            println!();
            println!();
            print!("Mode: {:?}", mode);
        }

        for tile_address in BACKTAB_ADDRESS..BACKTAB_ADDRESS + BACKTAB_SIZE {
            let index = tile_address - BACKTAB_ADDRESS;
            let x = index % BACKTAB_COLS;
            let y = index / BACKTAB_COLS;
            let tile = memory_lock.read().read(tile_address)?;
            let grom = tile & 0x0800 == 0;

            if self.debug == Debug::BACKTAB {
                if x == 0 {
                    println!();
                }
                print!("{:>04X} ", tile);
            }

            match mode {
                Mode::FGBG => self.draw_fg_bg(x, y, tile, grom, memory_lock)?,
                Mode::ColorStack if (tile & 0x1800) == 0x1000 => {
                    self.draw_colored_square(x, y, tile, memory_lock)?
                }
                Mode::ColorStack => self.draw_color_stack(x, y, tile, grom, memory_lock)?,
            }
        }

        Ok(())
    }

    fn draw_mobs(
        &mut self,
        frame_data: &mut FrameData,
        memory_lock: &RwLock<Memory>,
    ) -> Result<(), Error> {
        for index in 0..8 {
            let x_reg = frame_data.mob_x[index];
            let y_reg = frame_data.mob_y[index];
            let attr_reg = frame_data.mob_attr[index];

            let mut mob_data = MobData::load(index, x_reg, y_reg, attr_reg);
            let (card1, card2) = if mob_data.y_res {
                (
                    // If y_res is set the last bit of the card_id is ignored and the first
                    // card_id is always even and the second card_id is always odd
                    self.get_card(mob_data.grom, mob_data.card_id & 0xFFFE, memory_lock)?,
                    self.get_card(mob_data.grom, mob_data.card_id | 0x0001, memory_lock)
                        .ok(),
                )
            } else {
                (
                    self.get_card(mob_data.grom, mob_data.card_id, memory_lock)?,
                    None,
                )
            };

            mob_data.card1 = Some(card1);
            mob_data.card2 = card2;

            if mob_data.visible {
                self.draw_mob(&mob_data, frame_data);
            }
        }

        Ok(())
    }

    fn draw_mob(&mut self, mob_data: &MobData, frame_data: &mut FrameData) {
        // Mobs are essentially drawn at double resolution so we need to double their position.
        let x = mob_data.x * 2;
        let y = mob_data.y * 2;
        let y_scale = match mob_data.size {
            0x00 => 1,
            0x01 => 2,
            0x02 => 4,
            0x03 => 8,
            _ => 1,
        };
        let x_scale = if mob_data.double_wide { 2 } else { 1 };

        self.draw_mob_card(
            x,
            y,
            x_scale,
            y_scale,
            0,
            0,
            &mob_data.card1.unwrap(),
            mob_data,
            frame_data,
        );

        if let Some(card2) = mob_data.card2 {
            let y_step = match mob_data.size {
                0x00 => 8,
                0x01 => 16,
                0x02 => 32,
                0x03 => 64,
                _ => 8,
            };
            self.draw_mob_card(
                x, y, x_scale, y_scale, 0, y_step, &card2, mob_data, frame_data,
            );
        }
    }

    fn draw_mob_card(
        &mut self,
        x: usize,
        y: usize,
        x_scale: usize,
        y_scale: usize,
        x_step: usize,
        y_step: usize,
        card: &[u8; 8],
        mob_data: &MobData,
        frame_data: &mut FrameData,
    ) {
        let mut x_step = x_step;
        let mut y_step = y_step;
        let mut line = 0;
        for _ in 0..CARD_HEIGHT {
            for _ in 0..y_scale {
                for x_offset in 0..CARD_WIDTH {
                    for _ in 0..x_scale {
                        let mask = if mob_data.flip_x {
                            1 << x_offset
                        } else {
                            1 << (7 - x_offset)
                        };
                        let card_pixel = if mob_data.flip_y {
                            card[7 - line] & mask
                        } else {
                            card[line] & mask
                        };
                        if card_pixel != 0 {
                            let is_backgroud =
                                self.get_collision_info(x + x_step, y + y_step) == 0x08;
                            if !mob_data.priority | !is_backgroud {
                                self.draw_half_pixel(x + x_step, y + y_step, mob_data.color);
                            }
                            if mob_data.interactible {
                                self.set_mob_collision(
                                    x + x_step,
                                    y + y_step,
                                    mob_data.id as u8,
                                    frame_data,
                                );
                            }
                        }
                        x_step += 2;
                    }
                }
                x_step = 0;
                y_step += 1;
            }
            line += 1;
        }
    }

    fn load_frame(
        &self,
        frame_data: &FrameData,
        screen_lock: &RwLock<Pixels<fltk::window::DoubleWindow>>,
    ) {
        let mut screen = screen_lock.write();
        let frame = screen.get_frame();
        let x_offset = OBJECT_FIELD_X_OFFSET - frame_data.horz_delay as usize;
        let y_offset = OBJECT_FIELD_Y_OFFSET - frame_data.vert_delay as usize;

        for y in 0..SCREEN_HEIGHT as usize {
            for x in 0..SCREEN_WIDTH as usize {
                let frame_index = (y * SCREEN_WIDTH as usize + x) * 4;
                let object_field_x = x + x_offset;
                let object_field_y = y + y_offset;
                let object_field_index = (object_field_y * OBJECT_FIELD_WIDTH + object_field_x)
                    * OBJECT_FIELD_COLOR_BYTES;

                frame[frame_index] = self.object_field[object_field_index];
                frame[frame_index + 1] = self.object_field[object_field_index + 1];
                frame[frame_index + 2] = self.object_field[object_field_index + 2];
                frame[frame_index + 3] = 0xFF;
            }
        }
    }

    fn draw_collision_data(&mut self) {
        for y in 0..OBJECT_FIELD_HEIGHT as usize {
            for x in 0..OBJECT_FIELD_WIDTH as usize {
                let object_field_index = (y * OBJECT_FIELD_WIDTH as usize + x) * 3;
                let collision_buffer_index = y * OBJECT_FIELD_WIDTH as usize + x;

                let color_code = self.collision_buffer[collision_buffer_index];
                if color_code == 0xFF {
                    continue;
                }

                let Color(bytes) = Color::from_code(color_code);

                self.object_field[object_field_index] = bytes[0];
                self.object_field[object_field_index + 1] = bytes[1];
                self.object_field[object_field_index + 2] = bytes[2];
            }
        }
    }

    fn check_display_active(&self, signals_lock: &RwLock<Signals>) -> bool {
        let display_active = signals_lock.read().activate_display;
        signals_lock.write().activate_display = false;
        display_active
    }

    fn draw_fg_bg(
        &mut self,
        x: usize,
        y: usize,
        tile: u16,
        grom: bool,
        memory_lock: &RwLock<Memory>,
    ) -> Result<(), Error> {
        let fg_color = Color::from_code((tile & 0x0007) as u8);

        let mut bg_color_code = (tile >> 9) & 0x0003;
        bg_color_code |= (tile >> 11) & 0x0004;
        bg_color_code |= (tile >> 9) & 0x0008;
        let bg_color = Color::from_code(bg_color_code as u8);

        let card_id = (tile >> 3) & 0x003F;
        let card = self.get_card(grom, card_id, memory_lock)?;
        self.draw_tile(x, y, fg_color, bg_color, card);

        Ok(())
    }

    fn colored_square_color(
        &self,
        color_code: u8,
        memory_lock: &RwLock<Memory>,
    ) -> Result<Color, Error> {
        let mut color_code = color_code;
        if color_code == 0x07 {
            let color_stack_address = (0x0028 + self.color_stack_pointer) as usize;
            color_code = memory_lock.read().read(color_stack_address)? as u8;
        }
        Ok(Color::from_code(color_code))
    }

    fn draw_colored_square(
        &mut self,
        x: usize,
        y: usize,
        tile: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<(), Error> {
        println!("!!!!!!!!!! COLORED SQUARE !!!!!!!!!!");
        let color_0_code = tile & 0x0007;
        let color_1_code = (tile >> 3) & 0x0007;
        let color_2_code = (tile >> 6) & 0x0007;
        let mut color_3_code = (tile >> 9) & 0x0003;
        color_3_code |= (tile >> 13) & 0x0008;

        let color_0 = self.colored_square_color(color_0_code as u8, memory_lock)?;
        let color_1 = self.colored_square_color(color_1_code as u8, memory_lock)?;
        let color_2 = self.colored_square_color(color_2_code as u8, memory_lock)?;
        let color_3 = self.colored_square_color(color_3_code as u8, memory_lock)?;

        let x_start = x * 8;
        let x_end = x_start + 8;
        let y_start = y * 8;
        let y_end = y_start + 8;

        for y in y_start..y_end {
            for x in x_start..x_end {
                let (color_code, color) = match (x, y) {
                    (0..4, 0..4) => (color_0_code, color_0),
                    (4..8, 0..4) => (color_1_code, color_1),
                    (0..4, 4..8) => (color_2_code, color_2),
                    (4..8, 4..8) => (color_3_code, color_3),
                    _ => (0x07, Color::RED),
                };
                self.draw_background_pixel(x, y, color);

                if color_code != 0x07 {
                    self.set_background_collision(x, y);
                }
            }
        }

        Ok(())
    }

    fn get_top_color_stack(&self, memory_lock: &RwLock<Memory>) -> Result<Color, Error> {
        let color_stack_address = (0x0028 + self.color_stack_pointer) as usize;
        let color_code = memory_lock.read().read(color_stack_address)? as u8;
        Ok(Color::from_code(color_code))
    }

    fn draw_color_stack(
        &mut self,
        x: usize,
        y: usize,
        tile: u16,
        grom: bool,
        memory_lock: &RwLock<Memory>,
    ) -> Result<(), Error> {
        // Advance color stack?
        if tile & 0x2000 != 0 {
            self.color_stack_pointer = self.color_stack_pointer + 1 % 4;
        }

        let fg_color_code = ((tile >> 9 & 0x0008) | (tile & 0x0007)) as u8;
        let fg_color = Color::from_code(fg_color_code);
        let bg_color = self.get_top_color_stack(memory_lock)?;

        let card_id = if grom {
            (tile >> 3) & 0x00FF
        } else {
            (tile >> 3) & 0x003F
        };

        let card = self.get_card(grom, card_id, memory_lock)?;
        self.draw_tile(x, y, fg_color, bg_color, card);
        Ok(())
    }

    fn draw_tile(&mut self, x: usize, y: usize, fg_color: Color, bg_color: Color, card: [u8; 8]) {
        let x_start = x * 8;
        let x_end = x_start + 8;
        let y_start = y * 8;
        let y_end = y_start + 8;

        for y in y_start..y_end {
            for x in x_start..x_end {
                let line = (y - y_start) as usize;
                let mask = 1 << (7 - (x - x_start));
                let card_pixel = card[line] & mask;
                if card_pixel == 0 {
                    self.draw_background_pixel(x, y, bg_color);
                } else {
                    self.draw_background_pixel(x, y, fg_color);
                    self.set_background_collision(x, y);
                }
            }
        }
    }

    fn draw_background_pixel(&mut self, x: usize, y: usize, color: Color) {
        let x = (x + OBJECT_FIELD_X_OFFSET / 2) * 2;
        let y = (y + OBJECT_FIELD_Y_OFFSET / 2) * 2;
        let Color(bytes) = color;

        for y in y..=y + 1 {
            for x in x..=x + 1 {
                if x < OBJECT_FIELD_WIDTH && y < OBJECT_FIELD_HEIGHT {
                    let start = ((y * OBJECT_FIELD_WIDTH + x) * 3) as usize;
                    self.object_field[start] = bytes[0];
                    self.object_field[start + 1] = bytes[1];
                    self.object_field[start + 2] = bytes[2];
                }
            }
        }
    }

    fn set_background_collision(&mut self, x: usize, y: usize) {
        let x = (x + OBJECT_FIELD_X_OFFSET / 2) * 2;
        let y = (y + OBJECT_FIELD_Y_OFFSET / 2) * 2;

        for y in y..=y + 1 {
            for x in x..=x + 1 {
                if x < OBJECT_FIELD_WIDTH && y < OBJECT_FIELD_HEIGHT {
                    let index = (y * OBJECT_FIELD_WIDTH + x) as usize;
                    self.collision_buffer[index] = 0x08;
                }
            }
        }
    }

    fn draw_half_pixel(&mut self, x: usize, y: usize, color: Color) {
        let Color(bytes) = color;
        for x in x..=x + 1 {
            if x < OBJECT_FIELD_WIDTH && y < OBJECT_FIELD_HEIGHT {
                let start = (y * OBJECT_FIELD_WIDTH + x) * 3;
                self.object_field[start] = bytes[0];
                self.object_field[start + 1] = bytes[1];
                self.object_field[start + 2] = bytes[2];
            }
        }
    }

    fn set_mob_collision(&mut self, x: usize, y: usize, id: u8, frame_data: &mut FrameData) {
        if x < OBJECT_FIELD_WIDTH && y < OBJECT_FIELD_HEIGHT {
            let index = y * OBJECT_FIELD_WIDTH + x;
            let buffer_id = self.collision_buffer[index];
            if buffer_id != 0xFF {
                frame_data.mob_collision[id as usize] |= 1 << buffer_id;
                match buffer_id {
                    0..8 => {
                        frame_data.mob_collision[buffer_id as usize] |= 1 << id;
                    }
                    _ => (),
                }
            } else {
                self.collision_buffer[index] = id;
                self.collision_buffer[index + 1] = id;
            }
        }
    }

    fn get_collision_info(&mut self, x: usize, y: usize) -> u8 {
        if x < OBJECT_FIELD_WIDTH && y < OBJECT_FIELD_HEIGHT {
            let index = y * OBJECT_FIELD_WIDTH + x;
            self.collision_buffer[index]
        } else {
            0xFF
        }
    }

    fn get_card(
        &self,
        grom: bool,
        card_id: u16,
        memory_lock: &RwLock<Memory>,
    ) -> Result<[u8; 8], Error> {
        let card_address = if grom { GROM_ADDRESS } else { GRAM_ADDRESS };
        memory_lock.read().read_card(card_address, card_id)
    }
}
