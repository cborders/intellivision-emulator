#![feature(duration_consts_float)]
#![feature(exclusive_range_pattern)]
pub mod cp1610;
pub mod dasm;
pub mod memory;
pub mod psg;
pub mod rom;
pub mod stic;

use {
    anyhow::Error,
    argh::FromArgs,
    cp1610::CP1610,
    fltk::{app::*, window::*},
    memory::{Memory, Signals},
    parking_lot::RwLock,
    pixels::{Pixels, SurfaceTexture},
    psg::PSG,
    rom::{Game, Rom},
    std::{
        path::PathBuf,
        sync::{
            mpsc::{self, TryRecvError},
            Arc,
        },
        thread,
    },
    stic::Stic,
};

const WINDOW_WIDTH: i32 = stic::SCREEN_WIDTH as i32 * 2;
const WINDOW_HEIGHT: i32 = stic::SCREEN_HEIGHT as i32 * 2;

#[derive(PartialEq)]
pub enum Debug {
    None,
    CPU,
    BACKTAB,
    Collision,
}

impl std::str::FromStr for Debug {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.trim().to_lowercase().as_ref() {
            "cpu" => Ok(Debug::CPU),
            "backtab" => Ok(Debug::BACKTAB),
            "collision" => Ok(Debug::Collision),
            _ => Ok(Debug::None),
        }
    }
}

impl std::fmt::Debug for Debug {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            Debug::None => write!(f, "None"),
            Debug::CPU => write!(f, "CPU"),
            Debug::BACKTAB => write!(f, "BACKTAB"),
            Debug::Collision => write!(f, "Collision"),
        }
    }
}

#[derive(Debug, FromArgs)]
/// Configuration for the emulator
pub struct Config {
    /// path to the executive ROM
    #[argh(option, default = "PathBuf::from(\"bios/EXEC.int\")")]
    pub exec: PathBuf,

    /// path to the graphics ROM
    #[argh(option, default = "PathBuf::from(\"bios/GROM.int\")")]
    pub grom: PathBuf,

    /// show cpu debug information
    #[argh(option, default = "Debug::None", short = 'd')]
    pub debug: Debug,

    /// fullscreen flag
    #[argh(switch, short = 'f')]
    pub fullscreen: bool,

    /// path to the game ROM
    #[argh(positional)]
    pub game: PathBuf,
}

pub struct Intellivision {
    exec: Rom,
    grom: Rom,
}

impl Intellivision {
    pub fn new(exec: Rom, grom: Rom) -> Result<Intellivision, Error> {
        Ok(Intellivision { exec, grom })
    }

    pub fn run(&mut self, game: Game, config: Config) -> Result<(), Error> {
        let signals_lock = Arc::new(RwLock::new(Signals::new()));

        let memory_lock = Arc::new(RwLock::new(Memory::new(Arc::clone(&signals_lock))));
        {
            let mut memory = memory_lock.write();
            memory.load(&self.exec, 0x1000)?;
            memory.load_grom(&self.grom, 0x3000)?;
            memory.load_game(&game)?;
        }

        let (cpu_tx, cpu_rx) = mpsc::channel();
        let cpu_thread = {
            let memory_lock = Arc::clone(&memory_lock);
            let signals_lock = Arc::clone(&signals_lock);
            let debug = config.debug == Debug::CPU;
            thread::spawn(move || {
                let mut cpu = CP1610::new(debug);
                loop {
                    match cpu_rx.try_recv() {
                        Ok(_) | Err(TryRecvError::Disconnected) => {
                            println!("CPU: Shutting Down");
                            break;
                        }
                        _ => {
                            if cpu.tick(&memory_lock, &signals_lock).is_err() {
                                break;
                            }
                        }
                    }
                }
            })
        };

        let app = App::default();
        let mut window = Window::default()
            .with_label("Intellivator")
            .with_size(WINDOW_WIDTH, WINDOW_HEIGHT)
            .center_screen();

        window.make_resizable(true);
        window.size_range(stic::SCREEN_WIDTH as i32, stic::SCREEN_HEIGHT as i32, 0, 0);
        window.end();
        window.show();

        let screen_lock = {
            let surface_texture =
                SurfaceTexture::new(WINDOW_WIDTH as u32, WINDOW_HEIGHT as u32, &window);
            Arc::new(RwLock::new(Pixels::new(
                stic::SCREEN_WIDTH,
                stic::SCREEN_HEIGHT,
                surface_texture,
            )?))
        };

        let (stic_tx, stic_rx) = mpsc::channel();
        let stic_thread = {
            let memory_lock = Arc::clone(&memory_lock);
            let signals_lock = Arc::clone(&signals_lock);
            let screen_lock = Arc::clone(&screen_lock);
            thread::spawn(move || {
                let mut stic = Stic::new(config.debug);
                loop {
                    match stic_rx.try_recv() {
                        Ok(_) | Err(TryRecvError::Disconnected) => {
                            println!("STIC: Shutting down");
                            break;
                        }
                        _ => {
                            if stic
                                .draw(&screen_lock, &memory_lock, &signals_lock)
                                .is_err()
                            {
                                break;
                            }
                        }
                    }
                }
            })
        };

        let (psg_tx, psg_rx) = mpsc::channel();
        let psg_thread = {
            let memory_lock = Arc::clone(&memory_lock);
            thread::spawn(move || {
                let mut psg = PSG::new();
                loop {
                    match psg_rx.try_recv() {
                        Ok(_) | Err(TryRecvError::Disconnected) => {
                            println!("PSG: Shutting Down");
                            break;
                        }
                        _ => {
                            if psg.tick(&memory_lock).is_err() {
                                break;
                            }
                        }
                    }
                }
            })
        };

        {
            let screen_lock = Arc::clone(&screen_lock);
            window.handle2(move |window, event| match event {
                Event::Resize => {
                    let mut screen = screen_lock.write();
                    screen.resize(window.width() as u32, window.height() as u32);
                    if screen.render().is_err() {
                        false
                    } else {
                        true
                    }
                }
                _ => false,
            });
        }

        window.set_callback(move || {
            if event() == Event::Close || event_key() == Key::Escape {
                let _ = cpu_tx.send(());
                let _ = stic_tx.send(());
                let _ = psg_tx.send(());
                app.quit();
            }
        });

        while app.wait() {
            let mut controller: u16 = 0x00FF;
            // Number Pad
            if event_key_down(Key::from_char('1')) {
                controller &= !0x0081;
            }
            if event_key_down(Key::from_char('2')) {
                controller &= !0x0041;
            }
            if event_key_down(Key::from_char('3')) {
                controller &= !0x0021;
            }
            if event_key_down(Key::from_char('q')) {
                controller &= !0x0082;
            }
            if event_key_down(Key::from_char('w')) {
                controller &= !0x0042;
            }
            if event_key_down(Key::from_char('e')) {
                controller &= !0x0022;
            }
            if event_key_down(Key::from_char('a')) {
                controller &= !0x0084;
            }
            if event_key_down(Key::from_char('s')) {
                controller &= !0x0044;
            }
            if event_key_down(Key::from_char('d')) {
                controller &= !0x0024;
            }
            if event_key_down(Key::from_char('z')) {
                controller &= !0x0088;
            }
            if event_key_down(Key::from_char('x')) {
                controller &= !0x0048;
            }
            if event_key_down(Key::from_char('c')) {
                controller &= !0x0028;
            }
            // Full Direction Pad
            if event_key_down(Key::from_char('7')) {
                controller &= !0x000C;
            }
            if event_key_down(Key::from_char('8')) {
                controller &= !0x0004;
            }
            if event_key_down(Key::from_char('9')) {
                controller &= !0x0014;
            }
            if event_key_down(Key::from_char('0')) {
                controller &= !0x0016;
            }
            if event_key_down(Key::from_char('u')) {
                controller &= !0x001C;
            }
            if event_key_down(Key::from_char('i')) {
                controller &= !0x0018;
            }
            if event_key_down(Key::from_char('o')) {
                controller &= !0x0006;
            }
            if event_key_down(Key::from_char('p')) {
                controller &= !0x0002;
            }
            if event_key_down(Key::from_char('j')) {
                controller &= !0x0008;
            }
            if event_key_down(Key::from_char('k')) {
                controller &= !0x0009;
            }
            if event_key_down(Key::from_char('l')) {
                controller &= !0x0012;
            }
            if event_key_down(Key::from_char(';')) {
                controller &= !0x0013;
            }
            if event_key_down(Key::from_char('m')) {
                controller &= !0x0019;
            }
            if event_key_down(Key::from_char(',')) {
                controller &= !0x0011;
            }
            if event_key_down(Key::from_char('.')) {
                controller &= !0x0001;
            }
            if event_key_down(Key::from_char('/')) {
                controller &= !0x0003;
            }
            // Cardinal Directions with Arrows
            if event_key_down(Key::Up) {
                controller &= !0x0004;
            }
            if event_key_down(Key::Down) {
                controller &= !0x0001;
            }
            if event_key_down(Key::Left) {
                controller &= !0x0008;
            }
            if event_key_down(Key::Right) {
                controller &= !0x0002;
            }
            // Side Buttons
            if event_key_down(Key::from_char('v')) {
                controller &= !0x00A0;
            }
            if event_key_down(Key::from_char('b')) {
                controller &= !0x0060;
            }
            if event_key_down(Key::from_char('n')) {
                controller &= !0x00C0;
            }

            {
                let mut memory = memory_lock.write();
                if memory.write(0x01FE, controller).is_err() {
                    break;
                }

                if memory.write(0x01FF, 0x00FF).is_err() {
                    break;
                }
            }

            {
                let mut screen = screen_lock.write();
                screen.resize(window.width() as u32, window.height() as u32);
                if screen.render().is_err() {
                    break;
                }
            }

            window.redraw();
        }

        let _ = cpu_thread.join().unwrap();
        let _ = stic_thread.join().unwrap();
        let _ = psg_thread.join().unwrap();

        Ok(())
    }
}
