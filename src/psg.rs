use {
    crate::memory::Memory,
    anyhow::Error,
    parking_lot::RwLock,
    std::{
        thread,
        time::{Duration, Instant},
    },
};

// Clock Speed
// 3579545 Hz (NTSC)
// 4000000 Hz (PAL/SECAM)
const CLOCK_SPEED: f64 = 3579545.0;
const TARGET_TICK_TIME: Duration = Duration::from_secs_f64(1.0 / CLOCK_SPEED);

const PSG1_ADDRESS: usize = 0x01F0;
const PSG2_ADDRESS: usize = 0x00F0;
const REGISTER_COUNT: usize = 16;

pub struct PSG {
    needs_init: bool,
    tick_start: Instant,
}

impl PSG {
    pub fn new() -> Self {
        PSG {
            needs_init: true,
            tick_start: Instant::now(),
        }
    }

    pub fn tick(&mut self, memory_lock: &RwLock<Memory>) -> Result<(), Error> {
        if self.needs_init {
            let mut memory = memory_lock.write();

            // Clear PSG registers
            for address in PSG1_ADDRESS..PSG1_ADDRESS + REGISTER_COUNT {
                memory.write(address, 0x00FF)?;
            }

            for address in PSG2_ADDRESS..PSG2_ADDRESS + REGISTER_COUNT {
                memory.write(address, 0x00FF)?;
            }

            self.needs_init = false;
            self.tick_start = Instant::now();
        }

        if self.tick_start.elapsed() >= TARGET_TICK_TIME {
            self.tick_start = Instant::now();
        }

        thread::yield_now();
        Ok(())
    }
}
