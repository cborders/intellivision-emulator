use {
    anyhow::{Context, Error},
    argh,
    intv::{rom, Config, Intellivision},
};

fn main() -> Result<(), Error> {
    let config: Config = argh::from_env();

    let exec = rom::load_rom(&config.exec)
        .context(format!("Failed to load EXEC ROM at {:?}", &config.exec))?;
    let grom = rom::load_grom(&config.grom)
        .context(format!("Failed to load GROM as {:?}", &config.grom))?;
    let game =
        rom::load_game(&config.game).context(format!("Failed to load {:?}", &config.game))?;

    println!("Found EXEC ROM at {:?}", &config.exec);
    println!("Found GROM at {:?}", &config.grom);
    println!("Loading cartridge: {}", game.name);
    println!("Using Memory Map: {:?}", game.memory_mapping);

    let mut intellivision = Intellivision::new(exec, grom)?;
    intellivision.run(game, config)
}
