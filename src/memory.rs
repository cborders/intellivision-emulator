use {
    crate::{
        rom::{self, Game, Rom},
        stic,
    },
    anyhow::{anyhow, Error},
    parking_lot::RwLock,
    std::sync::Arc,
};

pub struct Signals {
    pub interrupt_request_masked: bool,
    pub interrupt_acknowledge: bool,
    pub activate_display: bool,
    pub stic_mode: stic::Mode,
}

impl Signals {
    pub fn new() -> Signals {
        Signals {
            interrupt_request_masked: false,
            interrupt_acknowledge: false,
            activate_display: false,
            stic_mode: stic::Mode::ColorStack,
        }
    }
}

pub struct Memory {
    pub storage: Vec<u16>,
    signals_lock: Arc<RwLock<Signals>>,
}

impl Memory {
    /// Initializes the memory block
    pub fn new(signals_lock: Arc<RwLock<Signals>>) -> Memory {
        Memory {
            storage: vec![0xFFFF; 0x10000],
            signals_lock,
        }
    }

    /// Loads the given Rom into memory at the specified location
    /// # Parameters
    /// - `rom`: the ROM to be loaded into memory
    /// - `location`: memory address where the rom should begin
    ///
    /// # Errors
    /// - When trying to write outside of the available memory.
    pub fn load(&mut self, rom: &Rom, location: u16) -> Result<(), Error> {
        if location as usize + rom.len() > self.storage.len() {
            Err(anyhow!("Failed loading ROM. Memory overflow."))
        } else {
            for index in 0..rom.len() {
                self.storage[location as usize + index] = rom[index];
            }

            Ok(())
        }
    }

    pub fn load_grom(&mut self, rom: &Rom, location: u16) -> Result<(), Error> {
        if location as usize + rom.len() > self.storage.len() {
            Err(anyhow!("Failed loading ROM. Memory overflow."))
        } else {
            for index in 0..rom.len() {
                self.storage[location as usize + index] = rom[index];
            }

            Ok(())
        }
    }

    /// Loads the given Game into memory at the specified location
    /// # Parameters
    /// - `game`: the Game to be loaded into memory
    ///
    /// # Errors
    /// - When trying to write outside of the available memory.
    pub fn load_game(&mut self, game: &Game) -> Result<(), Error> {
        for location in 0..game.rom.len() {
            let mapped_location = rom::map_address(&game, location)?;
            if mapped_location as usize > self.storage.len() {
                return Err(anyhow!("Failed loading ROM. Memory overflow."));
            } else {
                self.storage[mapped_location] = game.rom[location];
            }
        }

        Ok(())
    }

    /// Reads the value at the given location
    /// # Parameters
    /// - `location`: the memory address to read from
    ///
    /// # Returns
    /// - The value held in the requested location
    ///
    /// # Errors
    /// - When trying to read from an invalid location
    pub fn read(&self, location: usize) -> Result<u16, Error> {
        if location < self.storage.len() {
            match location {
                0x0021 | 0x4021 | 0x8021 | 0xC021 => {
                    self.signals_lock.write().stic_mode = stic::Mode::ColorStack
                }
                _ => (),
            }
            Ok(self.storage[location])
        } else {
            Err(anyhow!("Failed to read memory due to an underflow"))
        }
    }

    pub fn read_card(&self, location: usize, card_id: u16) -> Result<[u8; 8], Error> {
        // Cards are 8 x 8 pixels so they only use the lower 8 bits of each memory location
        let location = location + (card_id * 8) as usize;

        if location < self.storage.len() && location + 8 < self.storage.len() {
            let card_u16 = &self.storage[location..location + 8];

            let card: [u8; 8] = [
                (card_u16[0] & 0x00FF) as u8,
                (card_u16[1] & 0x00FF) as u8,
                (card_u16[2] & 0x00FF) as u8,
                (card_u16[3] & 0x00FF) as u8,
                (card_u16[4] & 0x00FF) as u8,
                (card_u16[5] & 0x00FF) as u8,
                (card_u16[6] & 0x00FF) as u8,
                (card_u16[7] & 0x00FF) as u8,
            ];
            Ok(card)
        } else {
            Err(anyhow!("Failed to read memory due to an underflow"))
        }
    }

    /// Reads the value at the given location
    /// # Parameters
    /// - `location`: the memory address to read from
    /// - `value`: the value to write at the requested location
    ///
    /// # Errors
    /// - When trying to write to an invalid location
    pub fn write(&mut self, location: usize, value: u16) -> Result<(), Error> {
        if location < self.storage.len() {
            // The STIC registers and the Graphics RAM are aliased to multiple locations. These
            // locations can be written but the original location is the only one that will return
            // the value when read.
            match location {
                // Scratchpad RAM is only 8 bits wide
                0x0100..=0x01EF => self.storage[location] = value & 0x00FF,
                // PSG registers are only 8 bits wide
                0x01F0..=0x01FF => self.storage[location] = value & 0x00FF,
                0x00F0..=0x00FF => self.storage[location] = value & 0x00FF,
                // STIC location and aliases
                0x0000..=0x003F => self.stic_write(location, value),
                0x4000..=0x403F => self.stic_write(location, value),
                0x8000..=0x803F => self.stic_write(location, value),
                0xC000..=0xC03F => self.stic_write(location, value),
                // GRAM location and aliases
                0x3800..=0x3FFF => self.gram_write(location, value),
                0x7800..=0x7FFF => self.gram_write(location, value),
                0xB800..=0xBFFF => self.gram_write(location, value),
                0xF800..=0xFFFF => self.gram_write(location, value),
                _ => self.storage[location] = value,
            }

            Ok(())
        } else {
            Err(anyhow!("Failed to write memory due to an overflow"))
        }
    }

    fn stic_write(&mut self, location: usize, value: u16) {
        let location_lsb = location & 0x00FF;
        self.storage[0x0000 | location_lsb] = value;

        match location_lsb {
            0x0020 => self.signals_lock.write().activate_display = true,
            0x0021 => self.signals_lock.write().stic_mode = stic::Mode::FGBG,
            _ => (),
        }
    }

    fn gram_write(&mut self, location: usize, value: u16) {
        let location_lsb = location & 0x0FFF;
        self.storage[0x3000 | location_lsb] = value;
    }
}
