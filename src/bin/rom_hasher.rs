use {
    anyhow::{anyhow, Context, Error},
    intv::rom,
    std::{env, fs, path::PathBuf},
};

const USAGE: &'static str = "Usage: rom_hasher <path>

Rom Hasher hashes game roms and outputs the Rust code to add the game to the database.

Options:
  --help            display usage information";

fn main() -> Result<(), Error> {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 || args[1] == "--help" {
        println!("{}", USAGE);
        return Ok(());
    }

    let path = PathBuf::from(&args[1]);
    for file in fs::read_dir(&path).context("Failed to load path")? {
        let path = file?.path();
        if let Some(extension) = path.extension() {
            match extension.to_str() {
                Some("int") | Some("bin") => (),
                Some("rom") => {
                    println!("Skipping {:?}. ROM files don't need to be hashed.", path);
                    continue;
                }
                _ => {
                    println!("Skipping {:?}. Unknown file extension.", path);
                    continue;
                }
            }
        } else {
            println!("Skipping {:?}. No file extension.", path);
            continue;
        }

        // Is this a known game?
        if let Ok(game) = rom::load_game(&path) {
            println!(
                "\"{:X}\" => Ok((MemoryMapping::{:?}, \"{}\".to_string())),",
                game.hash, game.memory_mapping, game.name
            );
        } else {
            let name = path
                .file_stem()
                .ok_or(anyhow!("Failed to get game name from: {:?}", path))?;
            let name = name
                .to_str()
                .ok_or(anyhow!("Failed to convert {:?} to a str", name))?;
            let rom = fs::read(&path).context("Failed to load ROM")?;
            let digest = md5::compute(rom);
            println!(
                "\"{:X}\" => Ok((MemoryMapping::MappingXXX, \"{}\".to_string())),",
                digest, name
            );
        }

        //"34B9E686AEE6F418D431A2F9A5A5C0CB" => Ok((MemoryMapping::Mapping10, "Pac-Man (1983) (Atarisoft)".to_string())),
    }

    Ok(())
}
