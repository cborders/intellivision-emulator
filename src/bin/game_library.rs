use {
    anyhow::{Context, Error},
    fltk::{app::*, button, table, window::*},
    intv::{rom, Config, Debug, Intellivision},
    std::{fs, path::PathBuf},
};

const SCREEN_WIDTH: i32 = 480;
const SCREEN_HEIGHT: i32 = 320;

#[derive(Clone, Debug)]
struct Game {
    name: String,
    path: PathBuf,
}

fn main() -> Result<(), Error> {
    // Look for games in the roms directory
    let mut library: Vec<Game> = vec![];

    for file in fs::read_dir("roms").context("Failed to load path")? {
        let path = file?.path();
        if let Some(extension) = path.extension() {
            match extension.to_str() {
                Some("int") | Some("bin") => (),
                Some("rom") => {
                    println!("Skipping {:?}. ROM files aren't supported yet.", path);
                    continue;
                }
                _ => {
                    println!("Skipping {:?}. Unknown file extension.", path);
                    continue;
                }
            }
        } else {
            println!("Skipping {:?}. No file extension.", path);
            continue;
        }

        let bin = fs::read(&path)?;
        let hash = md5::compute(&bin);
        if let Ok((_, name)) = rom::lookup_game(hash) {
            library.push(Game { name, path });
        }
    }

    // Build the UI
    let app = App::default();
    let mut window = Window::default()
        .with_label("Game Library")
        .with_size(SCREEN_WIDTH, SCREEN_HEIGHT)
        .center_screen();

    window.set_callback(move || {
        if event() == Event::Close || event_key() == Key::Escape {
            app.quit();
        }
    });

    let mut list = table::Table::default().size_of(&window);

    list.set_rows(library.len() as u32);
    list.set_cols(1);
    list.set_row_height_all(50);
    list.set_col_width_all(SCREEN_WIDTH);
    list.end();

    list.draw_cell2(move |t, ctx, row, _col, x, y, w, h| {
        if let table::TableContext::Cell = ctx {
            let game = library[row as usize].clone();
            let mut button = button::Button::new(x, y, w, h, &format!("{} {}", row, game.name));
            button.set_align(Align::Left | Align::Inside);
            button.set_callback(move || {
                launch_game(&game.path).expect(&format!("Failed to launch {}", game.name))
            });
            t.add(&button);
        }
    });

    window.end();
    window.show();
    while app.wait() {
        window.redraw();
    }

    Ok(())
}

fn launch_game(path: &PathBuf) -> Result<(), Error> {
    // TODO: Be more flexible here with naming.
    let exec_path = PathBuf::from("bios/EXEC.int");
    let grom_path = PathBuf::from("bios/GROM.int");
    let game_path = PathBuf::from(path);

    let fullscreen =
        cfg!(target_arch = "arm") && cfg!(target_os = "linux") && cfg!(target_env = "gnu");

    let config = Config {
        exec: exec_path,
        grom: grom_path,
        game: game_path,
        debug: Debug::None,
        fullscreen: fullscreen,
    };

    let exec = rom::load_rom(&config.exec)
        .context(format!("Failed to load EXEC ROM at {:?}", &config.exec))?;
    let grom = rom::load_grom(&config.grom)
        .context(format!("Failed to load GROM as {:?}", &config.grom))?;
    let game =
        rom::load_game(&config.game).context(format!("Failed to load {:?}", &config.game))?;

    let mut intellivision = Intellivision::new(exec, grom)?;
    intellivision.run(game, config)
}
