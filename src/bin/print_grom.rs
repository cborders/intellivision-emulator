use {
    anyhow::{Context, Error},
    intv::{
        memory::{Memory, Signals},
        rom,
    },
    parking_lot::RwLock,
    std::{env, path::PathBuf, sync::Arc},
};

const USAGE: &'static str = "Usage: print_grom <path>

Print the contents of the GROM

Options:
  --help            display usage information";

fn main() -> Result<(), Error> {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 || args[1] == "--help" {
        println!("{}", USAGE);
        return Ok(());
    }

    let path = PathBuf::from(&args[1]);
    let grom = rom::load_grom(&path).context("Failed to load GROM")?;

    let signals_lock = Arc::new(RwLock::new(Signals::new()));
    let mut memory = Memory::new(Arc::clone(&signals_lock));
    memory.load(&grom, 0)?;

    for row in 0..32 {
        for col in 0..8 {
            let card_id = row * 8 + col;
            print!("Card {:>3} ", card_id);
        }

        println!();
        for line in 0..8 {
            for col in 0..8 {
                let card_id = row * 8 + col;
                let card = memory.read_card(0, card_id)?;
                let byte = card[line];

                for pixel in 0..8 {
                    print!(
                        "{}",
                        if byte & (1 << 7 - pixel) != 0 {
                            "#"
                        } else {
                            "."
                        }
                    );
                }

                print!(" ");
            }
            println!();
        }
        println!();
    }

    Ok(())
}
