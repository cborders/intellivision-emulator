use {
    anyhow::{Context, Error},
    argh::FromArgs,
    intv::{dasm::Disassembler, rom},
    std::path::PathBuf,
};

#[derive(FromArgs, Debug)]
/// Dasm disassembles Intellivision ROMS
struct RomInfo {
    /// location that the ROM would be stored in memory
    #[argh(option, default = "String::from(\"0\")", short = 'l')]
    location: String,

    /// path to the ROM
    #[argh(positional)]
    path: PathBuf,
}

fn main() -> Result<(), Error> {
    let info: RomInfo = argh::from_env();
    let mut dasm = Disassembler::new();

    // Try to find a matching memory map for this ROM.
    if let Ok(game) = rom::load_game(&info.path) {
        dasm.dasm_game(&game);
    } else {
        let rom = rom::load_rom(&info.path).context("Failed to load ROM")?;
        dasm.dasm(rom, usize::from_str_radix(&info.location, 16)?);
    }
    Ok(())
}
