use {
    anyhow::{Context, Error},
    fltk::{app::App, window::*},
    intv::{
        memory::{Memory, Signals},
        rom,
        stic::{self, Stic},
        Debug,
    },
    parking_lot::RwLock,
    pixels::{Pixels, SurfaceTexture},
    std::{path::PathBuf, sync::Arc, thread},
};

const PREVIEW_WINDOW_WIDTH: u32 = stic::SCREEN_WIDTH * 2;
const PREVIEW_WINDOW_HEIGHT: u32 = stic::SCREEN_HEIGHT * 2;

const TOOL_PANEL_WIDTH: i32 = 200;
const WINDOW_WIDTH: i32 = PREVIEW_WINDOW_WIDTH as i32 + TOOL_PANEL_WIDTH;
const WINDOW_HEIGHT: i32 = PREVIEW_WINDOW_HEIGHT as i32;

const BACKTAB_ADDRESS: usize = 0x0200;
#[allow(dead_code)]
const BACKTAB_ROWS: usize = 12;
#[allow(dead_code)]
const BACKTAB_COLS: usize = 20;
#[allow(dead_code)]
const BACKTAB_SIZE: usize = BACKTAB_ROWS * BACKTAB_COLS;
#[allow(dead_code)]
const GROM_ADDRESS: usize = 0x3000;
const BORDER_COLOR_REGISTER: usize = 0x002C;
const COLOR_STACK_ADDRESS: usize = 0x0028;

fn main() -> Result<(), Error> {
    let app = App::default();
    let mut window = Window::new(100, 100, WINDOW_WIDTH, WINDOW_HEIGHT, "Screen Creator");

    let mut preview = Window::new(
        0,
        0,
        PREVIEW_WINDOW_WIDTH as i32,
        PREVIEW_WINDOW_HEIGHT as i32,
        " Preview Window",
    );
    preview.set_color(Color::Black);
    preview.end();

    window.end();
    window.show();

    let screen_lock = {
        let surface_texture =
            SurfaceTexture::new(PREVIEW_WINDOW_WIDTH, PREVIEW_WINDOW_HEIGHT, &preview);
        Arc::new(RwLock::new(Pixels::new(
            stic::SCREEN_WIDTH,
            stic::SCREEN_HEIGHT,
            surface_texture,
        )?))
    };

    let signals_lock = Arc::new(RwLock::new(Signals::new()));
    {
        let mut signals = signals_lock.write();
        signals.activate_display = true;
        signals.stic_mode = stic::Mode::ColorStack;
    }

    let default_screen: Vec<u16> = vec![
        0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
        0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
        0x052F, 0x0000, 0x052E, 0x0000, 0x052D, 0x0000, 0x052C, 0x0000, 0x0000, 0x0523, 0x0000,
        0x0522, 0x0000, 0x0521, 0x0000, 0x0520, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
        0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
        0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x816F, 0x820F, 0x82A7, 0x82A7, 0x822F,
        0x8267, 0x8007, 0x812F, 0x8267, 0x822F, 0x821F, 0x82A7, 0x8297, 0x827F, 0x8277, 0x824F,
        0x821F, 0x829F, 0x8007, 0x8007, 0x8007, 0x8007, 0x8007, 0x8007, 0x8007, 0x8287, 0x8297,
        0x822F, 0x829F, 0x822F, 0x8277, 0x82A7, 0x829F, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
        0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
        0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
        0x014F, 0x0177, 0x01A7, 0x012F, 0x0167, 0x0167, 0x014F, 0x01B7, 0x014F, 0x019F, 0x014F,
        0x017F, 0x0177, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0007, 0x0007,
        0x0007, 0x0007, 0x0007, 0x0007, 0x0007, 0x014F, 0x0177, 0x01A7, 0x012F, 0x0167, 0x0167,
        0x014F, 0x013F, 0x012F, 0x0177, 0x01A7, 0x0000, 0x0000, 0x0007, 0x0007, 0x0007, 0x0007,
        0x0007, 0x0007, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
        0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
        0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
        0x0000, 0x0000, 0x0000, 0x811F, 0x827F, 0x8287, 0x8297, 0x8007, 0x8107, 0x8007, 0x808F,
        0x80CF, 0x00BF, 0x00C7, 0x8007, 0x816F, 0x820F, 0x82A7, 0x82A7, 0x822F, 0x8267, 0x0000,
        0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
        0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
    ];

    let grom_path = PathBuf::from("bios/GROM.int");
    let grom =
        rom::load_grom(&grom_path).context(format!("Failed to load GROM as {:?}", grom_path))?;
    let memory_lock = Arc::new(RwLock::new(Memory::new(Arc::clone(&signals_lock))));
    {
        let mut memory = memory_lock.write();
        memory.load_grom(&grom, 0x3000)?;
        memory.write(BORDER_COLOR_REGISTER, 0x0005)?; // Green
        memory.write(COLOR_STACK_ADDRESS, 0x0005)?;
        memory.write(COLOR_STACK_ADDRESS + 1, 0x0005)?;
        memory.write(COLOR_STACK_ADDRESS + 2, 0x0005)?;
        memory.write(COLOR_STACK_ADDRESS + 3, 0x0005)?;
        for index in 0..default_screen.len() {
            let _ = memory.write(BACKTAB_ADDRESS + index, default_screen[index]);
        }
    }

    let stic_thread = {
        let memory_lock = Arc::clone(&memory_lock);
        let signals_lock = Arc::clone(&signals_lock);
        let screen_lock = Arc::clone(&screen_lock);
        thread::spawn(move || {
            let mut stic = Stic::new(Debug::None);
            loop {
                {
                    let mut signals = signals_lock.write();
                    signals.activate_display = true;
                    signals.stic_mode = stic::Mode::ColorStack;
                }

                {
                    let mut memory = memory_lock.write();
                    let _ = memory.write(BORDER_COLOR_REGISTER, 0x0005); // Green
                }

                if stic
                    .draw(&*screen_lock, &*memory_lock, &*signals_lock)
                    .is_err()
                {
                    break;
                }
            }
        })
    };

    while app.wait() {
        {
            if screen_lock.write().render().is_err() {
                break;
            }
        }
        window.redraw();
    }

    let _ = stic_thread.join().unwrap();

    Ok(())
}
