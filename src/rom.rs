use {
    anyhow::{anyhow, Error},
    std::{fs, path::PathBuf},
};

pub type Rom = Vec<u16>;

#[derive(Debug)]
pub enum MemoryMapping {
    Mapping0,
    Mapping1,
    Mapping2,
    Mapping3,
    Mapping4,
    Mapping5,
    Mapping6,
    Mapping7,
    Mapping8,
    Mapping9,
}

pub struct Game {
    pub rom: Rom,
    pub hash: md5::Digest,
    pub name: String,
    pub memory_mapping: MemoryMapping,
}

pub fn load_rom(path: &PathBuf) -> Result<Rom, Error> {
    let bin = fs::read(path)?;
    Ok(convert(&bin))
}

pub fn load_grom(path: &PathBuf) -> Result<Rom, Error> {
    let bin = fs::read(path)?;
    let rom = bin
        .chunks_exact(1)
        .into_iter()
        .map(|a| u16::from_be_bytes([0x00, a[0]]))
        .collect();

    Ok(rom)
}

pub fn load_game(path: &PathBuf) -> Result<Game, Error> {
    let bin = fs::read(path)?;
    let extension = path
        .extension()
        .ok_or(anyhow!("No file extension found in {:?}", path))?;
    match extension.to_str() {
        Some("int") | Some("bin") => load_game_bin(bin),
        Some("rom") => load_game_rom(bin),
        _ => return Err(anyhow!("Unknown file extension")),
    }
}

fn load_game_bin(bin: Vec<u8>) -> Result<Game, Error> {
    let hash = md5::compute(&bin);
    let rom = convert(&bin);
    let (memory_mapping, name) = lookup_game(hash)?;
    Ok(Game {
        rom,
        hash,
        memory_mapping,
        name,
    })
}

fn load_game_rom(_bin: Vec<u8>) -> Result<Game, Error> {
    Err(anyhow!("Rom loading is not yet supported."))
}

fn convert(bin: &Vec<u8>) -> Vec<u16> {
    bin.chunks_exact(2)
        .into_iter()
        .map(|a| u16::from_ne_bytes([a[1], a[0]]))
        .collect()
}

pub fn lookup_game(hash: md5::Digest) -> Result<(MemoryMapping, String), Error> {
    match format!("{:X}", hash).as_str() {
        "3DEA0DF016CA7F2C06C77373E3571F71" => Ok((
            MemoryMapping::Mapping0,
            "4-TRIS (2000) (Joseph Zbiciak)".to_string(),
        )),
        "F004411CA868FAF7EA416012772546FC" => Ok((
            MemoryMapping::Mapping0,
            "4-TRIS (2001) (Joseph Zbiciak)".to_string(),
        )),
        "0229A4889E18A37D1D631505D1CB83DF" => Ok((
            MemoryMapping::Mapping0,
            "Advanced D&D - Treasure of Tarmin (1982) (Mattel)".to_string(),
        )),
        "F1168C0894946C290685F133B81B4FAC" => Ok((
            MemoryMapping::Mapping0,
            "Advanced Dungeons and Dragons (1982) (Mattel)".to_string(),
        )),
        "17FD2AF3368C310FB7928DCFE0311514" => Ok((
            MemoryMapping::Mapping0,
            "Adventure (AD&D - Cloudy Mountain) (1982) (Mattel)".to_string(),
        )),
        "131AB07864114F44D4019939481EADD3" => Ok((
            MemoryMapping::Mapping0,
            "Air Strike (1982) (Mattel)".to_string(),
        )),
        "92E0826C7D677D4038764A7933696921" => Ok((
            MemoryMapping::Mapping0,
            "All-Star Major League Baseball (1983) (Mattel)".to_string(),
        )),
        "7FA60B1CB4ADAC3088727D5AC53EC183" => Ok((
            MemoryMapping::Mapping0,
            "Armor Battle (1978) (Mattel)".to_string(),
        )),
        "8F6A5B5B65D84F947236E503D378370B" => Ok((
            MemoryMapping::Mapping0,
            "Astrosmash (1981) (Mattel)".to_string(),
        )),
        "C7B00E08FECA5527C76F0EEA1BD9DB7E" => Ok((
            MemoryMapping::Mapping0,
            "Astrosmash - Meteor (1981) (Mattel)".to_string(),
        )),
        "317EF1EE4D12DF7CE7DD57DBC273B4B1" => Ok((
            MemoryMapping::Mapping7,
            "Atlantis (1981) (Imagic) [!]".to_string(),
        )),
        "97E865E173D137CF5576D080268F7DF5" => Ok((
            MemoryMapping::Mapping0,
            "Auto Racing (1979) (Mattel)".to_string(),
        )),
        "44FB31805C87974AC205871E61800A77" => Ok((
            MemoryMapping::Mapping0,
            "B-17 Bomber (1981) (Mattel) [!]".to_string(),
        )),
        "B88C2989496A5CA5E276D56A2EBFB2AB" => Ok((
            MemoryMapping::Mapping3,
            "Baseball (1978) (Mattel)".to_string(),
        )),
        "0CF15A41CCE922DBD7F0DEBFE0BFF792" => Ok((
            MemoryMapping::Mapping0,
            "BeamRider (1983) (Activision) [!]".to_string(),
        )),
        "F6D0E268EFE254DC4539FB6DF5A6CE24" => Ok((
            MemoryMapping::Mapping7,
            "Beauty and the Beast (1982) (Imagic) [!]".to_string(),
        )),
        "56FDA8DCF4920A182DE8F5BDD06590EA" => Ok((
            MemoryMapping::Mapping0,
            "Blockade Runner (1983) (Interphase)".to_string(),
        )),
        "8D0D0BB758718E663886A890D1412FD5" => Ok((
            MemoryMapping::Mapping0,
            "Bomb Squad (1982) (Mattel) [!]".to_string(),
        )),
        "4B29AFC041BF51B1AA0F6505A36D2C6A" => Ok((
            MemoryMapping::Mapping0,
            "Boxing (1980) (Mattel)".to_string(),
        )),
        "D9DF0B76EDA4F0E3E0BC5B645F1384FB" => Ok((
            MemoryMapping::Mapping0,
            "Brickout! (1981) (Mattel)".to_string(),
        )),
        "ED0D27C9494C8E06AEFA45AB5640F1F7" => Ok((
            MemoryMapping::Mapping0,
            "Bump 'N' Jump (1982-83) (Mattel)".to_string(),
        )),
        "D81075A4C254463724AC3BAB0B500A08" => Ok((
            MemoryMapping::Mapping0,
            "BurgerTime! (1982) (Mattel)".to_string(),
        )),
        "A7CFDDBCF7208FC84A793D12FFDE3C51" => Ok((
            MemoryMapping::Mapping0,
            "BurgerTime! - New Levels Hack (2002) (David Harley)".to_string(),
        )),
        "0032F034ED1D761D1F0DFF79115FEC78" => Ok((
            MemoryMapping::Mapping0,
            "Buzz Bombers (1982) (Mattel)".to_string(),
        )),
        "28EF20B81FD9EAC68A19FBFCAE5DAB9A" => Ok((
            MemoryMapping::Mapping0,
            "Carnival (1982) (Coleco-CBS)".to_string(),
        )),
        "F8044ADF6FCABEC7DB12B5F980B56FC2" => Ok((
            MemoryMapping::Mapping0,
            "Castle Trailer (2003) (Arnauld Chevallier)".to_string(),
        )),
        "8C57A45B86D907ECEBFF9EDC3A132AA5" => Ok((
            MemoryMapping::Mapping6,
            "Centipede (1983) (Atarisoft)".to_string(),
        )),
        "016BE420F1F20FC2ED8EC051E196AAEC" => Ok((
            MemoryMapping::Mapping1,
            "Championship Tennis (1985) (Mattel)".to_string(),
        )),
        "8122BB07816ACDCB3F0A037B72F02142" => Ok((
            MemoryMapping::Mapping0,
            "Checkers (1979) (Mattel)".to_string(),
        )),
        "9FF5833918A5ACCBFDDC856258BC2D3B" => Ok((
            MemoryMapping::Mapping2,
            "Commando (1987) (Mattel)".to_string(),
        )),
        "75276981F9EAB0DDC260FCF2331FA5D9" => Ok((
            MemoryMapping::Mapping5,
            "Congo Bongo (1983) (Sega)".to_string(),
        )),
        "B16191A056944EAB615BD73EB3235CAB" => Ok((
            MemoryMapping::Mapping0,
            "Crazy Clones (1981) (PD)".to_string(),
        )),
        "E89814FAB2AD565179511C592D524FC4" => Ok((
            MemoryMapping::Mapping5,
            "Defender (1983) (Atarisoft)".to_string(),
        )),
        "9855AEFCC210DF151BA3373D869E8B66" => Ok((
            MemoryMapping::Mapping7,
            "Demon Attack (1982) (Imagic) [!]".to_string(),
        )),
        "A8F2AEE64D0163309BF17E90C69FE5B9" => Ok((
            MemoryMapping::Mapping5,
            "Dig Dug (1987) (Intv Corp)".to_string(),
        )),
        "AE3B9621C32FD1D327E2AA994E69F62A" => Ok((
            MemoryMapping::Mapping2,
            "Diner (1987) (Intv Corp)".to_string(),
        )),
        "A43D060B5E7C6155F7469BF3DD245DE4" => Ok((
            MemoryMapping::Mapping0,
            "Donkey Kong (1982) (Coleco)".to_string(),
        )),
        "4F16342D203DA7AF060CC6EEC3D103F7" => Ok((
            MemoryMapping::Mapping0,
            "Donkey Kong Jr (1982) (Coleco)".to_string(),
        )),
        "A52574877A018EF1EC10E0FF3038FBB9" => Ok((
            MemoryMapping::Mapping0,
            "Dracula (1982) (Imagic) [!]".to_string(),
        )),
        "E4DC456380AC48EA36C3B46CB8E2FB51" => Ok((
            MemoryMapping::Mapping0,
            "Dragonfire (1982) (Imagic) [!]".to_string(),
        )),
        "5DA863A6BB5CAB4E6E3B0B422124B789" => Ok((
            MemoryMapping::Mapping0,
            "Dreadnaught Factor, The (1983) (Activision) [!]".to_string(),
        )),
        "EC25DD8CAC7287B2E7DB017EE9FC2ED8" => Ok((
            MemoryMapping::Mapping0,
            "Dreadnaught Factor, The (1983) (Activision) [a1]".to_string(),
        )),
        "9D0564C5882C0BBD72B3415D06C13C39" => Ok((
            MemoryMapping::Mapping0,
            "Dreadnaught Factor, The (Prototype) (1983) (Activision)".to_string(),
        )),
        "0960019B1AFCF56BAA851D36FAC4D295" => Ok((
            MemoryMapping::Mapping0,
            "Duncan's Thin Ice (1983) (Mattel)".to_string(),
        )),
        "DF373C83FF0815A605F6796FF9CBF70B" => Ok((
            MemoryMapping::Mapping0,
            "Easter Eggs (1981) (Mattel)".to_string(),
        )),
        "DCBEA7CDE7D03F099680531FE7A80AB5" => Ok((
            MemoryMapping::Mapping0,
            "Electric Company - Math Fun (1978) (CTW)".to_string(),
        )),
        "36FF2340FF5972F9AA4D353B633E082E" => Ok((
            MemoryMapping::Mapping0,
            "Electric Company - Word Fun (1980) (CTW)".to_string(),
        )),
        "C13B1E3B0D33908C0FC073F2032D9BE9" => Ok((
            MemoryMapping::Mapping0,
            "Fathom (1983) (Imagic) [!]".to_string(),
        )),
        "BB806B41644E33B6FC5A4D9D3F8C7AEE" => Ok((
            MemoryMapping::Mapping0,
            "Frog Bog (1982) (Mattel)".to_string(),
        )),
        "523D46BCAB6C0C11F0672562FE31CD47" => Ok((
            MemoryMapping::Mapping0,
            "Frogger (1983) (Parker Bros)".to_string(),
        )),
        "11F588E42DB055B0803A51AC8A50F339" => Ok((
            MemoryMapping::Mapping9,
            "Game Factory (Prototype) (1983) (Mattel) [!]".to_string(),
        )),
        "7AF892D254145B8766B1BDBE016EC8E0" => Ok((
            MemoryMapping::Mapping0,
            "Grid Shock (1982) (Mattel)".to_string(),
        )),
        "C76D599EE3036BA40AA6B8FBA2630081" => Ok((
            MemoryMapping::Mapping0,
            "Groovy! (1999) (JRMZ Electronics)".to_string(),
        )),
        "9B697D6A408FE0D0CEBC89AE8D482161" => Ok((
            MemoryMapping::Mapping0,
            "Happy Trails (1983) (Activision) [o1]".to_string(),
        )),
        "01348CED597A48A4EE87A5B3683616CB" => Ok((
            MemoryMapping::Mapping0,
            "Happy Trails (1983) (Activision)".to_string(),
        )),
        "C4268724C89F44AE71A81400C2EA1A71" => Ok((
            MemoryMapping::Mapping0,
            "Hard Hat (1979) (Mattel)".to_string(),
        )),
        "96D52AC29D5D4FD50B69675885801C9A" => Ok((
            MemoryMapping::Mapping0,
            "Horse Racing (1980) (Mattel)".to_string(),
        )),
        "436CACF587E988368974FE273BE78474" => Ok((
            MemoryMapping::Mapping2,
            "Hover Force (1986) (Intv Corp)".to_string(),
        )),
        "F92D9F1D08B97383F9A954BA887CECB0" => Ok((
            MemoryMapping::Mapping0,
            "Hypnotic Lights (1981) (Mattel)".to_string(),
        )),
        "79D475FF853683FFD9BE630628A491FA" => Ok((
            MemoryMapping::Mapping0,
            "Ice Trek (1983) (Imagic) [!]".to_string(),
        )),
        "D3AE61336D17EC08D0414E7702F2408E" => Ok((
            MemoryMapping::Mapping0,
            "INTV - Intelligent TV Demo #1682 (Prototype) (1978) (Mattel)".to_string(),
        )),
        "39BEAD8336C98F005B0FF05DAF5A0F90" => Ok((
            MemoryMapping::Mapping1,
            "INTV - Intelligent TV Demo #5853 (1983) (Mattel) [o1]".to_string(),
        )),
        "AADC44BFBB9A0A1A6A0D29C8044368C6" => Ok((
            MemoryMapping::Mapping1,
            "INTV - Intelligent TV Demo #5853 (1983) (Mattel)".to_string(),
        )),
        "32955D443A8143AE3B6D99760F58B054" => Ok((
            MemoryMapping::Mapping0,
            "INTV - Intelligent TV Demo #5932 Revised (1978) (Mattel) [o1]".to_string(),
        )),
        "10FF772D1850EC292984762030E8202A" => Ok((
            MemoryMapping::Mapping0,
            "INTV - Intelligent TV Demo #5932 Revised (1978) (Mattel)".to_string(),
        )),
        "B9591D68433C4D0CF92BD2C2EBFDD2BB" => Ok((
            MemoryMapping::Mapping0,
            "INTV - Intelligent TV Demo (1978) (Mattel)".to_string(),
        )),
        "1BCA1222C1B778D3CF223D658DD82AF8" => Ok((
            MemoryMapping::Mapping0,
            "INTV - Intelligent TV Demo Intl. #5859 (1982) (Mattel) [o1]".to_string(),
        )),
        "A5A6D3B3C00225EE712822C8EB286354" => Ok((
            MemoryMapping::Mapping0,
            "INTV - Intelligent TV Demo Intl. #5859 (1982) (Mattel)".to_string(),
        )),
        "88F4166655918655B389DAC4DAE67C34" => Ok((
            MemoryMapping::Mapping0,
            "Jetsons, The - Ways With Words (1983) (Mattel)".to_string(),
        )),
        "EAB911E041089584DE216BC3FEAA5C9A" => Ok((
            MemoryMapping::Mapping1,
            "King of the Mountain (1982) (Mattel)".to_string(),
        )),
        "C02D360393D2CD73A91EE56C56809410" => Ok((
            MemoryMapping::Mapping0,
            "Kool-Aid Man (1983) (Mattel)".to_string(),
        )),
        "B512D919BF3DB6F1E6B9A80835E032EC" => Ok((
            MemoryMapping::Mapping0,
            "Lady Bug (1983) (Coleco)".to_string(),
        )),
        "712FA2E78A5D8B02C9DE067B798E2C69" => Ok((
            MemoryMapping::Mapping4,
            "Land Battle (1982) (Mattel)".to_string(),
        )),
        "6506A1BF0C749FC768CB6B5162E0CBD7" => Ok((
            MemoryMapping::Mapping0,
            "Las Vegas Blackjack and Poker (1979) (Mattel)".to_string(),
        )),
        "203EFD0D5B6C14B88FD5C7C0ED6BFC16" => Ok((
            MemoryMapping::Mapping0,
            "Las Vegas Roulette (1979) (Mattel)".to_string(),
        )),
        "080F03A856158F713166E2CF4CAD670D" => Ok((
            MemoryMapping::Mapping0,
            "League of Light (Prototype) (1983) (Activision) [!]".to_string(),
        )),
        "8DFC158F903621A7947FE886308D9E88" => Ok((
            MemoryMapping::Mapping0,
            "League of Light (Prototype) (1983) (Activision) [a1][!]".to_string(),
        )),
        "407200059843E9FE63FCCB1498138863" => Ok((
            MemoryMapping::Mapping0,
            "League of Light (Prototype) (1983) (Activision) [a2]".to_string(),
        )),
        "1E661B21B1CD968632CD5BA3BE8B589A" => Ok((
            MemoryMapping::Mapping2,
            "Learning Fun I - Math Master Factor Fun (1987) (Intv Corp)".to_string(),
        )),
        "718D9E11B01629BC46CDE657DF3D1460" => Ok((
            MemoryMapping::Mapping2,
            "Learning Fun II - Word Wizard Memory Fun (1987) (Intv Corp)".to_string(),
        )),
        "92C9CE0470CFC12E20DF5B0246C60FEA" => Ok((
            MemoryMapping::Mapping0,
            "Lock 'N' Chase (1982) (Mattel)".to_string(),
        )),
        "FF8355790CEB7CE6905DD27C8510CD78" => Ok((
            MemoryMapping::Mapping0,
            "Loco-Motion (1982) (Mattel)".to_string(),
        )),
        "7D709DFC4A08230402078AED6C4CBB68" => Ok((
            MemoryMapping::Mapping0,
            "Maze Demo #1 (2000) (JRMZ Electronics)".to_string(),
        )),
        "C122F4A85F6F5A4DAEF63175899E29E5" => Ok((
            MemoryMapping::Mapping0,
            "Maze Demo #2 (2000) (JRMZ Electronics)".to_string(),
        )),
        "0F0969F9716A3D5D1D867AE99323AE7D" => Ok((
            MemoryMapping::Mapping0,
            "Melody Blaster (1983) (Mattel) [!]".to_string(),
        )),
        "137D6101EC260405D3D5339822E172F7" => Ok((
            MemoryMapping::Mapping7,
            "Microsurgeon (1982) (Imagic) [!]".to_string(),
        )),
        "CBD28F3573AB61FDDE21CABB7659CEAA" => Ok((
            MemoryMapping::Mapping0,
            "Mind Strike! (1982) (Mattel) [!]".to_string(),
        )),
        "F5AD906C25CC2AB000F2DBE55E1D5236" => Ok((
            MemoryMapping::Mapping0,
            "Minotaur (1981) (Mattel)".to_string(),
        )),
        "4012D161292EE63379E07D09F73217D1" => Ok((
            MemoryMapping::Mapping0,
            "Minotaur (Treasure of Tarmin Hack) (1982) (Mattel)".to_string(),
        )),
        "B88190CD36A0D8103D3A2D635AF9A459" => Ok((
            MemoryMapping::Mapping0,
            "Minotaur V1.1 (1981) (Mattel) [!]".to_string(),
        )),
        "580FA4904FE1C660CEFD86EFD841A6B9" => Ok((
            MemoryMapping::Mapping0,
            "Minotaur V2 (1981) (Mattel) [!]".to_string(),
        )),
        "30F68D218BAE18B69DAEB9E273FDC9D0" => Ok((
            MemoryMapping::Mapping0,
            "Mission X (1982) (Mattel)".to_string(),
        )),
        "1AB93DAB53B52EA6AA7D964682DFCF03" => Ok((
            MemoryMapping::Mapping0,
            "Motocross (1982) (Mattel)".to_string(),
        )),
        "1247C5F449BB1EFCB5D63225A0D57A2D" => Ok((
            MemoryMapping::Mapping0,
            "Mouse Trap (1982) (Coleco)".to_string(),
        )),
        "828101531D09E38DAC4CB31954B5AE6E" => Ok((
            MemoryMapping::Mapping0,
            "Mr. Basic Meets Bits 'N Bytes (1983) (Mattel) [!]".to_string(),
        )),
        "E9E7BDA7061F0ED2442D2F63D69907EA" => Ok((
            MemoryMapping::Mapping0,
            "Mr. Basic Meets Bits 'N Bytes (1983) (Mattel) [b1]".to_string(),
        )),
        "EC08C8432F695446BCB78411775FA8B4" => Ok((
            MemoryMapping::Mapping0,
            "NASL Soccer (1979) (Mattel)".to_string(),
        )),
        "EFAFF20E6BBEB707B2486D031E3E1078" => Ok((
            MemoryMapping::Mapping0,
            "NBA Basketball (1978) (Mattel)".to_string(),
        )),
        "D0EF7A007E8635A13942FB6DA48012A2" => Ok((
            MemoryMapping::Mapping0,
            "NFL Football (1978) (Mattel)".to_string(),
        )),
        "6307B4EA467874B393A9A3F07F3EB50F" => Ok((
            MemoryMapping::Mapping0,
            "NHL Hockey (1979) (Mattel)".to_string(),
        )),
        "B5E9A146F4F04DAAB3BA68B0EC45958A" => Ok((
            MemoryMapping::Mapping0,
            "Night Stalker (1982) (Mattel)".to_string(),
        )),
        "74EDFE78C836AC3844E7676BFE61EFC5" => Ok((
            MemoryMapping::Mapping0,
            "Nova Blast (1983) (Imagic) [!]".to_string(),
        )),
        "F11987699A66C59AC874A50A5D905647" => Ok((
            MemoryMapping::Mapping0,
            "Number Jumble (1983) (Mattel) [!]".to_string(),
        )),
        "34B9E686AEE6F418D431A2F9A5A5C0CB" => Ok((
            MemoryMapping::Mapping5,
            "Pac-Man (1983) (Atarisoft)".to_string(),
        )),
        "8CD6023881AB241B3216D38C89F1D088" => Ok((
            MemoryMapping::Mapping5,
            "Pac-Man (1983) (Intv Corp)".to_string(),
        )),
        "26B658BB3F761EE0D50E93CB782E08EC" => Ok((
            MemoryMapping::Mapping0,
            "PBA Bowling (1980) (Mattel)".to_string(),
        )),
        "B707C1D38C0C21A7513D1DC0739CDF12" => Ok((
            MemoryMapping::Mapping0,
            "PGA Golf (1979) (Mattel)".to_string(),
        )),
        "2828CE0B76C7F8A5E3A4569FAC5C8EB9" => Ok((
            MemoryMapping::Mapping0,
            "Pinball (1981) (Mattel)".to_string(),
        )),
        "16E989656458AFD35D3C2E1AAA950697" => Ok((
            MemoryMapping::Mapping0,
            "Pitfall! (1982) (Activision) [!]".to_string(),
        )),
        "20A828F38DF6569B9B6E16D58D0C37C8" => Ok((
            MemoryMapping::Mapping2,
            "Pole Position (1986) (Intv Corp)".to_string(),
        )),
        "5BDCF45165F46D5DE3C3D43D9E95DA64" => Ok((
            MemoryMapping::Mapping0,
            "Pong (1999) (Joseph Zbiciak)".to_string(),
        )),
        "9E8AA1AB70B221E320191AE4B71F9EE2" => Ok((
            MemoryMapping::Mapping0,
            "Popeye (1983) (Parker Bros)".to_string(),
        )),
        "1C697DF0BEB93EEC95407BF87285A58A" => Ok((
            MemoryMapping::Mapping0,
            "Q-bert (1983) (Parker Bros)".to_string(),
        )),
        "61D8DD664D255DFCA43F8B40802D76AE" => Ok((
            MemoryMapping::Mapping0,
            "Reversi (1984) (Mattel)".to_string(),
        )),
        "13B129AD0FCDB79911CCBDF6B741201F" => Ok((
            MemoryMapping::Mapping0,
            "River Raid (1982-83) (Activision) [!]".to_string(),
        )),
        "9801155EDC40F19197BB2216611713E7" => Ok((
            MemoryMapping::Mapping0,
            "River Raid V1 (Prototype) (1982-83) (Activision)".to_string(),
        )),
        "380F8F83EF63B2C6C1CD92759BA9B92D" => Ok((
            MemoryMapping::Mapping0,
            "Robot Rubble V1 (Prototype) (1983) (Activision) [o1]".to_string(),
        )),
        "C58665B14AA2FB3003C3D153AA37984E" => Ok((
            MemoryMapping::Mapping0,
            "Robot Rubble V1 (Prototype) (1983) (Activision)".to_string(),
        )),
        "69C4E7BD55D70A04C4CDF48FB5170CF7" => Ok((
            MemoryMapping::Mapping0,
            "Robot Rubble V2 (Prototype) (1983) (Activision)".to_string(),
        )),
        "4F20EB32CEBCF182A57D28482A8FC4AD" => Ok((
            MemoryMapping::Mapping0,
            "Robot Rubble V3 (Prototype) (1983) (Activision) [!]".to_string(),
        )),
        "BCF1B30BA80303B36B1CC3BDD5042C1E" => Ok((
            MemoryMapping::Mapping0,
            "Royal Dealer (1981) (Mattel)".to_string(),
        )),
        "80A64F49D53BB0C2401FBFD4B6FD6E19" => Ok((
            MemoryMapping::Mapping0,
            "Safecracker (1983) (Imagic) [!]".to_string(),
        )),
        "FB5B1D020E643681CB9BF1B956DB0435" => Ok((
            MemoryMapping::Mapping0,
            "Santa's Helper (1983) (Mattel)".to_string(),
        )),
        "CB590A4BB37D38EC7E96CF140C6E649D" => Ok((
            MemoryMapping::Mapping0,
            "Scooby Doo's Maze Chase (1983) (Mattel)".to_string(),
        )),
        "4E9A8366EB0BB166DAD7A7385417EAC8" => Ok((
            MemoryMapping::Mapping0,
            "Sea Battle (1980) (Mattel)".to_string(),
        )),
        "5E04A5B21A18170207E157390BC54185" => Ok((
            MemoryMapping::Mapping0,
            "Sewer Sam (1983) (Interphase)".to_string(),
        )),
        "6FBEC0115AFFB97D26DA3D1985A4BF3F" => Ok((
            MemoryMapping::Mapping0,
            "Shark! Shark! (1982) (Mattel) [!]".to_string(),
        )),
        "9F85B961E6C40C2C786026F460AE02FA" => Ok((
            MemoryMapping::Mapping0,
            "Shark! Shark! (1982) (Mattel) [a1]".to_string(),
        )),
        "A1C80E87C8B8BF9549B30E78027F137A" => Ok((
            MemoryMapping::Mapping0,
            "Sharp Shot (1982) (Mattel)".to_string(),
        )),
        "6B7D47DB4B5BCC2C3D2696FBF65555A1" => Ok((
            MemoryMapping::Mapping2,
            "Slam Dunk - Super Pro Basketball (1987) (Intv Corp)".to_string(),
        )),
        "5EC328D358FF8C9664E4AB1DEAA89F80" => Ok((
            MemoryMapping::Mapping0,
            "Slap Shot - Super Pro Hockey (1987) (Intv Corp)".to_string(),
        )),
        "DF286E44CB487DAB088A43D22AF8CCFB" => {
            Ok((MemoryMapping::Mapping0, "Snafu (1981) (Mattel)".to_string()))
        }
        "FF0BAAFF4CA87535F826AD4B5CA3F814" => Ok((
            MemoryMapping::Mapping0,
            "Song Player - Back in the USSR (1999) (Joseph Zbiciak)".to_string(),
        )),
        "E823B09B162C299692C5DDC5FC7DD53F" => Ok((
            MemoryMapping::Mapping0,
            "Song Player - Closing Time (2001) (Joseph Zbiciak)".to_string(),
        )),
        "E42B401D3D665CD76DB7320D2E6B71E0" => Ok((
            MemoryMapping::Mapping0,
            "Song Player - Copacabana (1999) (Joseph Zbiciak)".to_string(),
        )),
        "C64F5E61C1B8DCFF557AA81F72DD0F3B" => Ok((
            MemoryMapping::Mapping0,
            "Song Player - Creep (1999) (Joseph Zbiciak)".to_string(),
        )),
        "3E77B2CCC47B78D6CDC1993A0938DB8C" => Ok((
            MemoryMapping::Mapping0,
            "Song Player - Nut March (1999) (Joseph Zbiciak)".to_string(),
        )),
        "765C3276E28B6293B79D04E5BF67ECB9" => Ok((
            MemoryMapping::Mapping0,
            "Song Player - Nut Reed (1999) (Joseph Zbiciak)".to_string(),
        )),
        "6C4C5E815FA6E15C40F50DC728D8DD18" => Ok((
            MemoryMapping::Mapping0,
            "Song Player - Nut Trep (1999) (Joseph Zbiciak)".to_string(),
        )),
        "C71B15A0F9E0787D79D03BB04D0D67FC" => Ok((
            MemoryMapping::Mapping0,
            "Song Player - Nut Waltz (1999) (Joseph Zbiciak)".to_string(),
        )),
        "33D5341C96172DAFDBAF6C487F2EE5F9" => Ok((
            MemoryMapping::Mapping0,
            "Song Player - Secret Agent Man (1999) (Joseph Zbiciak)".to_string(),
        )),
        "C81A2F16C0DB4EEE16D798E658DCF55B" => Ok((
            MemoryMapping::Mapping0,
            "Song Player - Take on Me 3 (1999) (Joseph Zbiciak)".to_string(),
        )),
        "9BA5144F1774D46FAB54BE8DF01DD3E6" => Ok((
            MemoryMapping::Mapping0,
            "Song Player - Take on Me 6 (1999) (Joseph Zbiciak)".to_string(),
        )),
        "4ECBB5EEDE54B3D784FD305DC07D9095" => Ok((
            MemoryMapping::Mapping0,
            "Song Player - Too Sexy (1999) (Joseph Zbiciak)".to_string(),
        )),
        "B7C771A6062EE673936008754C987FD1" => Ok((
            MemoryMapping::Mapping0,
            "Space Armada (1981) (Mattel)".to_string(),
        )),
        "8C11519753896450E6F38B8E338CB1F6" => Ok((
            MemoryMapping::Mapping0,
            "Space Battle (1979) (Mattel)".to_string(),
        )),
        "976C2CC2329565AA078E51EA97C7A76C" => Ok((
            MemoryMapping::Mapping0,
            "Space Cadet (1982) (Mattel)".to_string(),
        )),
        "20BA9224386C5F64B62B38FDF12589DB" => Ok((
            MemoryMapping::Mapping0,
            "Space Hawk (1981) (Mattel)".to_string(),
        )),
        "628F7614CC3912F5C33B1052E6C61F04" => Ok((
            MemoryMapping::Mapping0,
            "Space Spartans (1981) (Mattel) [!]".to_string(),
        )),
        "FA7BF5DB6680D9464161914917E16F1F" => Ok((
            MemoryMapping::Mapping2,
            "Spiker! - Super Pro Volleyball (1988) (Intv Corp)".to_string(),
        )),
        "547AAF6281FB4878007BC632F5F051B6" => Ok((
            MemoryMapping::Mapping2,
            "Stadium Mud Buggies (1988) (Intv Corp)".to_string(),
        )),
        "E39A9065388384CF439DC1484013D993" => Ok((
            MemoryMapping::Mapping0,
            "Stampede (1982) (Activision) [!]".to_string(),
        )),
        "F76A250066E55817F42872083BC854B2" => Ok((
            MemoryMapping::Mapping0,
            "Star Strike (1981) (Mattel)".to_string(),
        )),
        "DD7BC297265CF2F65062966312B0B966" => Ok((
            MemoryMapping::Mapping0,
            "Sub Hunt (1981) (Mattel)".to_string(),
        )),
        "F94C5691D97E77CD675C7CD65A19E8E4" => Ok((
            MemoryMapping::Mapping0,
            "Super Cobra (1983) (Konami)".to_string(),
        )),
        "AAC27C4E64E947B67AFBE05DB1F458CD" => Ok((
            MemoryMapping::Mapping0,
            "Super Masters! (1982) (Mattel)".to_string(),
        )),
        "EF9C76117F52AB8DAE9E305C4FDD769E" => Ok((
            MemoryMapping::Mapping2,
            "Super Pro Decathlon (1988) (Intv Corp)".to_string(),
        )),
        "64A2772852B1796BE8554A39301BF255" => Ok((
            MemoryMapping::Mapping2,
            "Super Pro Football (1986) (Intv Corp)".to_string(),
        )),
        "2C4B44090566720364A5A4627B73390A" => Ok((
            MemoryMapping::Mapping0,
            "Super Soccer (1983) (Mattel)".to_string(),
        )),
        "1F35F843EC58CC700CDA9E3ED35CDABE" => Ok((
            MemoryMapping::Mapping0,
            "Swords and Serpents (1982) (Imagic) [!]".to_string(),
        )),
        "7589DB92726D2F6B4C4D9749B4A2C707" => Ok((
            MemoryMapping::Mapping0,
            "Takeover (1982) (Mattel)".to_string(),
        )),
        "DCF742ED6EE2101769A48FAF45145F2C" => Ok((
            MemoryMapping::Mapping0,
            "Tennis (1980) (Mattel)".to_string(),
        )),
        "8C554FD4D06624BF878F3D50F5B59463" => Ok((
            MemoryMapping::Mapping0,
            "Thin Ice (Prototype) (1983) (Intv Corp) [!]".to_string(),
        )),
        "603AD521602531B5C9AD3E77AC47E590" => Ok((
            MemoryMapping::Mapping0,
            "Thunder Castle (1982) (Mattel)".to_string(),
        )),
        "95B106ACAECAB4FE3D43970EAE893452" => Ok((
            MemoryMapping::Mapping3,
            "Tower of Doom (1986) (Intv Corp)".to_string(),
        )),
        "DF8BF33772EE8A983EE67687A53A0D4A" => Ok((
            MemoryMapping::Mapping0,
            "Triple Action (1981) (Mattel)".to_string(),
        )),
        "BB2B785135B610AAE525FB92ED83E851" => Ok((
            MemoryMapping::Mapping9,
            "Triple Challenge (1986) (Intv Corp)".to_string(),
        )),
        "A0AE3C350ACB9B9BC51128011483D4BE" => Ok((
            MemoryMapping::Mapping0,
            "TRON - Deadly Discs (1981) (Mattel)".to_string(),
        )),
        "251EAE6057E3B55773748C39850CBCBE" => Ok((
            MemoryMapping::Mapping0,
            "TRON - Deadly Discs - Deadly Dogs (1987) (Intv Corp)".to_string(),
        )),
        "780A2BE0E38AC7FD6A7A6D350CD18284" => Ok((
            MemoryMapping::Mapping0,
            "TRON - Maze-A-Tron (1981) (Mattel)".to_string(),
        )),
        "E9E3C2A1C3690748D34F711CEAB625CD" => Ok((
            MemoryMapping::Mapping0,
            "TRON - Solar Sailer (1982) (Mattel)".to_string(),
        )),
        "E5836F66ACD6BA7B31C7DCCDA23B34DC" => Ok((
            MemoryMapping::Mapping0,
            "Tropical Trouble (1982) (Imagic) [!]".to_string(),
        )),
        "DCA85BCEA1F05FB81D38826433152BE0" => Ok((
            MemoryMapping::Mapping0,
            "Truckin' (1983) (Imagic) [!]".to_string(),
        )),
        "38A04C6D7BB006435CD2C3B28755396C" => {
            Ok((MemoryMapping::Mapping0, "Turbo (1983) (Coleco)".to_string()))
        }
        "BE9E56495AF99F3F29EF3BC3B0A51FD4" => Ok((
            MemoryMapping::Mapping0,
            "Tutankham (1983) (Parker Bros)".to_string(),
        )),
        "04004FF8ABF2BF4EE5992A0A8F20D3C4" => Ok((
            MemoryMapping::Mapping0,
            "U.S. Ski Team Skiing (1980) (Mattel)".to_string(),
        )),
        "11B861F65C37A59E67FF028435AC2025" => Ok((
            MemoryMapping::Mapping4,
            "USCF Chess (1981) (Mattel)".to_string(),
        )),
        "B93B1169C33B7220887AC7AF54224976" => Ok((
            MemoryMapping::Mapping0,
            "Utopia (1981) (Mattel)".to_string(),
        )),
        "4C149598BE3A6C3974FF5F90005484FC" => Ok((
            MemoryMapping::Mapping0,
            "Vectron (1982) (Mattel)".to_string(),
        )),
        "45B6EBFCECDBA7730DE30C5F95005CA6" => Ok((
            MemoryMapping::Mapping0,
            "Venture (1982) (Coleco)".to_string(),
        )),
        "7DBE8519C0ED760B0AA88C5769B1AFD1" => Ok((
            MemoryMapping::Mapping0,
            "White Water! (1983) (Imagic) [!]".to_string(),
        )),
        "DF8FF041F1F3C9DFAD0CF651C67799CA" => Ok((
            MemoryMapping::Mapping0,
            "World Cup Football (1985) (Nice Ideas)".to_string(),
        )),
        "2A107D2D1E71A0DD403A8D287A6E08BC" => Ok((
            MemoryMapping::Mapping1,
            "World Series Major League Baseball (1983) (Mattel) [!]".to_string(),
        )),
        "A9E4DEF140F13BAED21C093BF1D501E7" => Ok((
            MemoryMapping::Mapping1,
            "World Series Major League Baseball (1983) (Mattel) [b1]".to_string(),
        )),
        "49999333CF35D0CB321802CA378A7AF7" => Ok((
            MemoryMapping::Mapping0,
            "Worm Whomper (1983) (Activision) [!]".to_string(),
        )),
        "4C5C490B225748CC6777AFE3AF0D74CB" => Ok((
            MemoryMapping::Mapping0,
            "Zaxxon (1982) (Coleco)".to_string(),
        )),
        _ => Err(anyhow!("Attempted to load unknown game ROM.")),
    }
}

pub fn map_address(game: &Game, address: usize) -> Result<usize, Error> {
    match &game.memory_mapping {
        MemoryMapping::Mapping0 => match address {
            0x0000..=0x1FFF => Ok(address + 0x5000),
            0x2000..=0x2FFF => Ok(address - 0x2000 + 0xD000),
            0x3000..=0x3FFF => Ok(address - 0x3000 + 0xF000),
            _ => Err(anyhow!("Address: {} is out of mapping range", address)),
        },
        MemoryMapping::Mapping1 => match address {
            0x0000..=0x1FFF => Ok(address + 0x5000),
            0x2000..=0x4FFF => Ok(address - 0x2000 + 0xD000),
            _ => Err(anyhow!("Address: {} is out of mapping range", address)),
        },
        MemoryMapping::Mapping2 => match address {
            0x0000..=0x1FFF => Ok(address + 0x5000),
            0x2000..=0x4FFF => Ok(address - 0x2000 + 0x9000),
            0x5000..=0x5FFF => Ok(address - 0x5000 + 0xD000),
            _ => Err(anyhow!("Address: {} is out of mapping range", address)),
        },
        MemoryMapping::Mapping3 => match address {
            0x0000..=0x1FFF => Ok(address + 0x5000),
            0x2000..=0x3FFF => Ok(address - 0x2000 + 0x9000),
            0x4000..=0x4FFF => Ok(address - 0x4000 + 0xD000),
            0x5000..=0x5FFF => Ok(address - 0x5000 + 0xF000),
            _ => Err(anyhow!("Address: {} is out of mapping range", address)),
        },
        MemoryMapping::Mapping4 => match address {
            0x0000..=0x1FFF => Ok(address + 0x5000),
            // RAM $D000 - $D3FF
            _ => Err(anyhow!("Address: {} is out of mapping range", address)),
        },
        MemoryMapping::Mapping5 => match address {
            0x0000..=0x2FFF => Ok(address + 0x5000),
            0x3000..=0x5FFF => Ok(address - 0x3000 + 0x9000),
            _ => Err(anyhow!("Address: {} is out of mapping range", address)),
        },
        MemoryMapping::Mapping6 => match address {
            0x0000..=0x1FFF => Ok(address + 0x6000),
            _ => Err(anyhow!("Address: {} is out of mapping range", address)),
        },
        MemoryMapping::Mapping7 => match address {
            0x0000..=0x1FFF => Ok(address + 0x4800),
            _ => Err(anyhow!("Address: {} is out of mapping range", address)),
        },
        MemoryMapping::Mapping8 => match address {
            0x0000..=0x0FFF => Ok(address + 0x5000),
            0x1000..=0x1FFF => Ok(address - 0x1000 + 0x7000),
            _ => Err(anyhow!("Address: {} is out of mapping range", address)),
        },
        MemoryMapping::Mapping9 => match address {
            0x0000..=0x1FFF => Ok(address + 0x5000),
            0x2000..=0x3FFF => Ok(address - 0x2000 + 0x9000),
            0x4000..=0x4FFF => Ok(address - 0x4000 + 0xD000),
            0x5000..=0x5FFF => Ok(address - 0x5000 + 0xF000),
            // RAM $8800 - $8FFF
            _ => Err(anyhow!("Address: {} is out of mapping range", address)),
        },
    }
}
