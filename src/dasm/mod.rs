use crate::rom::{self, Game, Rom};

pub struct Disassembler {
    pub pc: usize,
    location: usize,
    pub double_byte: u8,
}

impl Disassembler {
    pub fn new() -> Disassembler {
        Disassembler {
            pc: 0,
            location: 0,
            double_byte: 0,
        }
    }

    pub fn dasm_game(&mut self, game: &Game) {
        self.dasm_impl(&game.rom, |location| {
            rom::map_address(game, location as usize).expect("Failed to map address") as u16
        });
    }

    pub fn dasm(&mut self, rom: Rom, location: usize) {
        self.location = location;
        self.dasm_impl(&rom, |location| location);
    }

    fn dasm_impl(&mut self, rom: &Rom, location_modifier: impl Fn(u16) -> u16) {
        while let Some(opcode) = self.read_opcode(&rom) {
            let pc = location_modifier((self.pc - 1 + self.location) as u16);
            let decle2 = self.peek_decle(self.pc, &rom);
            let decle3 = self.peek_decle(self.pc + 1, &rom);

            let (command, arg1, arg2, decles_used) = self.dasm_opcode(opcode, decle2, decle3);

            self.pc += decles_used as usize;

            if self.double_byte != 0 {
                self.double_byte -= 1;
            }

            print!("{} ", self.hex_address(pc).unwrap());
            print!("{:<8}", command);
            print!("{:<5}", arg1.unwrap_or("".to_string()));
            print!("{:<5}; ", arg2.unwrap_or("".to_string()));

            print!("{} ", self.hex_address(pc).unwrap());
            print!("{} ", self.hex(opcode).unwrap());

            if decles_used >= 1 {
                print!("{} ", self.hex(decle2.unwrap()).unwrap());
            }

            if decles_used == 2 {
                print!("{} ", self.hex(decle3.unwrap()).unwrap());
            }

            println!();
        }
    }

    pub fn dasm_opcode(
        &mut self,
        opcode: u16,
        decle2: Option<u16>,
        decle3: Option<u16>,
    ) -> (String, Option<String>, Option<String>, u16) {
        match opcode {
            0x0000 => ("HLT".to_string(), None, None, 0),
            0x0001 => {
                self.double_byte = 2;
                ("SDBD".to_string(), None, None, 0)
            }
            0x0002 => ("EIS".to_string(), None, None, 0),
            0x0003 => ("DIS".to_string(), None, None, 0),
            0x0004 => self.jump(decle2.unwrap(), decle3.unwrap()),
            0x0005 => ("TCI".to_string(), None, None, 0),
            0x0006 => ("CLRC".to_string(), None, None, 0),
            0x0007 => ("SETC".to_string(), None, None, 0),
            0x0008..=0x000F => ("INCR".to_string(), self.decode_register(opcode), None, 0),
            0x0010..=0x0017 => ("DECR".to_string(), self.decode_register(opcode), None, 0),
            0x0018..=0x001F => ("COMR".to_string(), self.decode_register(opcode), None, 0),
            0x0020..=0x0027 => ("NEGR".to_string(), self.decode_register(opcode), None, 0),
            0x0028..=0x002F => ("ADCR".to_string(), self.decode_register(opcode), None, 0),
            0x0030..=0x0033 => (
                "GSWD".to_string(),
                self.decode_register_subset(opcode),
                None,
                0,
            ),
            0x0034..=0x0035 => ("NOP".to_string(), None, None, 0),
            0x0036..=0x0037 => ("SIN".to_string(), None, None, 0),
            0x0038..=0x003F => ("RSWD".to_string(), self.decode_register(opcode), None, 0),
            0x0040..=0x0047 => self.shift_bytes("SWAP", opcode),
            0x0048..=0x004F => self.shift_bytes("SLL", opcode),
            0x0050..=0x0057 => self.shift_bytes("RLC", opcode),
            0x0058..=0x005F => self.shift_bytes("SLLC", opcode),
            0x0060..=0x0067 => self.shift_bytes("SLR", opcode),
            0x0068..=0x006F => self.shift_bytes("SAR", opcode),
            0x0070..=0x0077 => self.shift_bytes("RRC", opcode),
            0x0078..=0x007F => self.shift_bytes("SARC", opcode),
            0x0080..=0x00BF => {
                let (source, dest) = self.decode_source_dest(opcode);

                if source == dest {
                    ("TSTR".to_string(), source, None, 0)
                } else if dest.as_deref().unwrap() == "R7" {
                    ("JR".to_string(), source, None, 0)
                } else {
                    ("MOVR".to_string(), source, dest, 0)
                }
            }
            0x00C0..=0x00FF => self.register_bin_op("ADDR", opcode),
            0x0100..=0x013F => self.register_bin_op("SUBR", opcode),
            0x0140..=0x017F => self.register_bin_op("CMPR", opcode),
            0x0180..=0x01BF => self.register_bin_op("ANDR", opcode),
            0x01C0..=0x01FF => {
                let (source, dest) = self.decode_source_dest(opcode);
                if source == dest {
                    ("CLRR".to_string(), source, None, 0)
                } else {
                    ("XORR".to_string(), source, dest, 0)
                }
            }
            0x0200..=0x023F => self.branch(opcode, decle2.unwrap()),
            0x0240..=0x0247 => self.direct_mode_write("MVO", opcode, decle2.unwrap()),
            0x0248..=0x027F => self.move_out_indirect(opcode, decle2),
            0x0280..=0x0287 => self.direct_mode_read("MVI", opcode, decle2.unwrap()),
            0x0288..=0x02BF => self.move_in_indirect(opcode, decle2, decle3),
            0x02C0..=0x02C7 => self.direct_mode_read("ADD", opcode, decle2.unwrap()),
            0x02C8..=0x02FF => self.indirect_mode_op("ADD", opcode, decle2, decle3),
            0x0300..=0x0307 => self.direct_mode_read("SUB", opcode, decle2.unwrap()),
            0x0308..=0x033F => self.indirect_mode_op("SUB", opcode, decle2, decle3),
            0x0340..=0x0347 => self.direct_mode_read("CMP", opcode, decle2.unwrap()),
            0x0348..=0x037F => self.indirect_mode_op("CMP", opcode, decle2, decle3),
            0x0380..=0x0387 => self.direct_mode_read("AND", opcode, decle2.unwrap()),
            0x0388..=0x03BF => self.indirect_mode_op("AND", opcode, decle2, decle3),
            0x03C0..=0x03C7 => self.direct_mode_read("XOR", opcode, decle2.unwrap()),
            0x03C8..=0x03FF => self.indirect_mode_op("XOR", opcode, decle2, decle3),
            _ => ("Invalid opcode:".to_string(), self.hex(opcode), None, 0),
        }
    }

    fn read_opcode(&mut self, rom: &Rom) -> Option<u16> {
        if self.pc < rom.len() {
            let opcode = rom[self.pc];
            self.pc += 1;
            Some(opcode)
        } else {
            None
        }
    }

    fn peek_decle(&self, location: usize, rom: &Rom) -> Option<u16> {
        if location < rom.len() {
            Some(rom[location])
        } else {
            None
        }
    }

    fn jump(&self, decle2: u16, decle3: u16) -> (String, Option<String>, Option<String>, u16) {
        let reg = decle2 >> 8;
        let flags = decle2 & 0x0003;
        let address = (decle2 << 8) & 0xFC00 | decle3;

        match flags {
            0 if reg == 3 => ("J".to_string(), self.hex_address(address), None, 2),
            0 => (
                "JSR".to_string(),
                self.jump_register(reg),
                self.hex_address(address),
                2,
            ),
            1 if reg == 3 => ("JE".to_string(), self.hex_address(address), None, 2),
            1 => (
                "JSRE".to_string(),
                self.jump_register(reg),
                self.hex_address(address),
                2,
            ),
            2 if reg == 3 => ("JD".to_string(), self.hex_address(address), None, 2),
            2 => (
                "JSRD".to_string(),
                self.jump_register(reg),
                self.hex_address(address),
                2,
            ),
            _ => ("UNKNOWN".to_string(), self.hex(decle2), self.hex(decle3), 2),
        }
    }

    fn shift_bytes(
        &self,
        command: &str,
        opcode: u16,
    ) -> (String, Option<String>, Option<String>, u16) {
        let count = if opcode & 0x0004 == 0 {
            None
        } else {
            Some("2".to_string())
        };
        (
            command.to_string(),
            self.decode_register_subset(opcode),
            count,
            0,
        )
    }

    fn register_bin_op(
        &self,
        command: &str,
        opcode: u16,
    ) -> (String, Option<String>, Option<String>, u16) {
        let (source, dest) = self.decode_source_dest(opcode);
        (command.to_string(), source, dest, 0)
    }

    fn direct_mode_read(
        &self,
        command: &str,
        opcode: u16,
        value: u16,
    ) -> (String, Option<String>, Option<String>, u16) {
        (
            command.to_string(),
            self.hex(value),
            self.decode_register(opcode),
            1,
        )
    }

    fn direct_mode_write(
        &self,
        command: &str,
        opcode: u16,
        value: u16,
    ) -> (String, Option<String>, Option<String>, u16) {
        (
            command.to_string(),
            self.decode_register(opcode),
            self.hex(value),
            1,
        )
    }

    fn indirect_mode_op(
        &mut self,
        command: &str,
        opcode: u16,
        decle2: Option<u16>,
        decle3: Option<u16>,
    ) -> (String, Option<String>, Option<String>, u16) {
        let (address, dest) = self.decode_source_dest(opcode);
        match address.as_deref().unwrap() {
            "R7" => {
                let (value, decles) = if self.double_byte != 0 {
                    ((decle3.unwrap() << 8) | (decle2.unwrap() & 0x00FF), 2)
                } else {
                    (decle2.unwrap(), 1)
                };
                (format!("{}I", command), self.hex(value), dest, decles)
            }
            _ => (format!("{}@", command), address, dest, 0),
        }
    }

    fn move_out_indirect(
        &mut self,
        opcode: u16,
        decle2: Option<u16>,
    ) -> (String, Option<String>, Option<String>, u16) {
        let (address, source) = self.decode_source_dest(opcode);
        match address.as_deref().unwrap() {
            "R6" => ("PSHR".to_string(), source, None, 0),
            "R7" => ("MVOI".to_string(), source, self.hex(decle2.unwrap()), 1),
            _ => ("MVO@".to_string(), source, address, 0),
        }
    }

    fn move_in_indirect(
        &mut self,
        opcode: u16,
        decle2: Option<u16>,
        decle3: Option<u16>,
    ) -> (String, Option<String>, Option<String>, u16) {
        let (address, dest) = self.decode_source_dest(opcode);
        match address.as_deref().unwrap() {
            "R6" => ("PULR".to_string(), dest, None, 0),
            "R7" => {
                let (value, decles) = if self.double_byte != 0 {
                    ((decle3.unwrap() << 8) | (decle2.unwrap() & 0x00FF), 2)
                } else {
                    (decle2.unwrap(), 1)
                };
                ("MVII".to_string(), self.hex(value), dest, decles)
            }
            _ => ("MVI@".to_string(), address, dest, 0),
        }
    }

    fn branch(&self, opcode: u16, decle2: u16) -> (String, Option<String>, Option<String>, u16) {
        let forward = opcode & 0x0020 == 0;
        let external = opcode & 0x0010 != 0;
        let negate = opcode & 0x0008 != 0;
        let condition = (opcode & 0x0007) as u8;

        if external {
            ("BEXT".to_string(), None, None, 1)
        } else {
            let target = if forward {
                (self.location + self.pc) as u16 + decle2 + 1
            } else {
                let start = (self.location + self.pc) as u16;
                if let Some(value) = start.checked_sub(decle2) {
                    value
                } else {
                    0xFFFF - (decle2 - start) + 1
                }
            };

            match condition {
                // Branch Unconditionally
                0x00 if negate => ("NOPP".to_string(), None, None, 1),
                0x00 => ("B".to_string(), self.hex(target), None, 1),
                // Branch on carry
                0x01 if negate => ("BNC".to_string(), self.hex(target), None, 1),
                0x01 => ("BC".to_string(), self.hex(target), None, 1),
                // Branch on overflow
                0x02 if negate => ("BNOV".to_string(), self.hex(target), None, 1),
                0x02 => ("BOV".to_string(), self.hex(target), None, 1),
                // Branch on sign cleared
                0x03 if negate => ("BMI".to_string(), self.hex(target), None, 1),
                0x03 => ("BPL".to_string(), self.hex(target), None, 1),
                // Branch on zero
                0x04 if negate => ("BNEQ".to_string(), self.hex(target), None, 1),
                0x04 => ("BEQ".to_string(), self.hex(target), None, 1),
                // Branch on sign != overflow
                0x05 if negate => ("BGE".to_string(), self.hex(target), None, 1),
                0x05 => ("BLT".to_string(), self.hex(target), None, 1),
                // Branch on zero | sign != overflow
                0x06 if negate => ("BGT".to_string(), self.hex(target), None, 1),
                0x06 => ("BLE".to_string(), self.hex(target), None, 1),
                // Branch on sign != carry
                0x07 if negate => ("BESC".to_string(), self.hex(target), None, 1),
                0x07 => ("BUSC".to_string(), self.hex(target), None, 1),
                _ => ("UNKNOWN".to_string(), self.hex(opcode), self.hex(decle2), 1),
            }
        }
    }

    fn jump_register(&self, reg: u16) -> Option<String> {
        match reg {
            0 => Some("R4".to_string()),
            1 => Some("R5".to_string()),
            2 => Some("R6".to_string()),
            _ => Some("UNKNOWN".to_string()),
        }
    }

    fn decode_register(&self, opcode: u16) -> Option<String> {
        match opcode & 0x0007 {
            0 => Some("R0".to_string()),
            1 => Some("R1".to_string()),
            2 => Some("R2".to_string()),
            3 => Some("R3".to_string()),
            4 => Some("R4".to_string()),
            5 => Some("R5".to_string()),
            6 => Some("R6".to_string()),
            7 => Some("R7".to_string()),
            _ => Some("UNKNOWN".to_string()),
        }
    }

    fn decode_register_subset(&self, opcode: u16) -> Option<String> {
        match opcode & 0x0003 {
            0 => Some("R0".to_string()),
            1 => Some("R1".to_string()),
            2 => Some("R2".to_string()),
            3 => Some("R3".to_string()),
            _ => Some("UNKNOWN".to_string()),
        }
    }

    fn decode_source_dest(&self, opcode: u16) -> (Option<String>, Option<String>) {
        let source = match (opcode >> 3) & 0x0007 {
            0 => "R0",
            1 => "R1",
            2 => "R2",
            3 => "R3",
            4 => "R4",
            5 => "R5",
            6 => "R6",
            7 => "R7",
            _ => "UNKNOWN",
        };

        let dest = match opcode & 0x0007 {
            0 => "R0",
            1 => "R1",
            2 => "R2",
            3 => "R3",
            4 => "R4",
            5 => "R5",
            6 => "R6",
            7 => "R7",
            _ => "UNKNOWN",
        };

        (Some(source.to_string()), Some(dest.to_string()))
    }

    fn hex(&self, value: u16) -> Option<String> {
        Some(format!("{:>04X}", value))
    }

    fn hex_address(&self, value: u16) -> Option<String> {
        Some(format!("${:>04X}", value))
    }
}
