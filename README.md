# Intellivision Emulator
![Adventure](images/Adventure.png)
![Mountains](images/Mountains.png)
![Burger Time](images/BurgerTime.png)

## Building
### Prerequisites
You must have rustup and cmake installed.

### Rust Nightly
The CPU (CP1610) using duration consts and the STIC uses exclusive ranges which
are both currently a nightly, experimental feature. This means that you will
need to install the nightly version of the Rust compiler. Use the following
rustup command to do this:

```bash
rustup install nightly
```

After installing the nightly build, you have two choices. If you want to use the
nightly sdk for all of your projects you can set that as the default. This will
tell cargo to use the nightly sdk whenever you use a cargo command.

```bash
cargo default nightly
```

Now, anytime you run `cargo`, it will default to the nightly sdk so you can just
use the following command to build.

```bash
cargo build
```

The other option is to leave the stable sdk as the default and just use the
nightly sdk to build the emulator. To do that you need to tell rustup to use the
nightly build every time you run a cargo command.

```bash
rustup run nightly cargo build
rustup run nightly cargo run ...
```

## Running the Emulator
By default the emulator will look for the EXEC bin and the GROM in a directory
called `bios` in the same directory as the executable.

```text
|- emulator
|- bios
    |
    |- EXEC.int
    |- GROM.int
```

If you have these is a different location you will need to give the emulator
their full path using the `--exec` and `--grom` arguments.

```text
Usage: intv <game> [-e <exec>] [-g <grom>] [-d]

Configuration for the emulator

Options:
  -e, --exec        path to the executive ROM
  -g, --grom        path to the graphics ROM
  -d, --debug       should print debug information
  --help            display usage information
```

## Key Mapping
The current key mapping is hard coded for now. The controller number pad is
mapped to the following keys:

```none
+-+-+-+
|1|2|3|
+-+-+-+
|Q|W|E|
+-+-+-+
|A|S|D|
+-+-+-+
|Z|X|C|
+-+-+-+
```

The side buttons are mapped to V, B, and N.

The main directions (up, down, left, and right) are mapped to the arrow keys.

## Other Tools
### Disassembler
The disassembler will load a bin / int and see if it matches a known game. If so
it will load the memory map for that game and proceed with disassembly using the
correct memory locations. If no mapping is found it will use the location passed
in or zero if that option was omitted.

```text
Usage: dasm <path> [-l <location>]

Dasm disassembles Intellivision ROMS

Options:
  -l, --location    location that the ROM would be stored in memory
  --help            display usage information
```

### Rom Hasher
This is a tool used to add games to the library of known memory mappings.

```text
Usage: rom_hasher <path>

Rom Hasher hashes game roms and outputs the Rust code to add the game to the database.

Options:
  --help            display usage information
```

### Print GROM
This is a tool to display the contents of the Graphics ROM

```text
Usage: print_grom <path>

Print the contents of the GROM

Options:
  --help            display usage information
```
